import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

class ConfigGeneral extends Component {

  render(){
    return(
      //MENU SIDEBAR
      <div id="config-tool" className="closed">
        <a id="config-tool-cog">
          <i className="fa fa-cog"></i>
        </a>

        <div id="config-tool-options">
          <h4>Layout Options</h4>
          <ul>
            <li>
              <div className="checkbox-nice">
                <input type="checkbox" id="config-fixed-header" />
                <label for="config-fixed-header">
                  Fixed Header
                </label>
              </div>
            </li>
            <li>
              <div className="checkbox-nice">
                <input type="checkbox" id="config-fixed-sidebar" />
                <label for="config-fixed-sidebar">
                  Fixed Left Menu
                </label>
              </div>
            </li>
            <li>
              <div className="checkbox-nice">
                <input type="checkbox" id="config-fixed-footer" />
                <label for="config-fixed-footer">
                  Fixed Footer
                </label>
              </div>
            </li>
            <li>
              <div className="checkbox-nice">
                <input type="checkbox" id="config-boxed-layout" />
                <label for="config-boxed-layout">
                  Boxed Layout
                </label>
              </div>
            </li>
            <li>
              <div className="checkbox-nice">
                <input type="checkbox" id="config-rtl-layout" />
                <label for="config-rtl-layout">
                  Right-to-Left
                </label>
              </div>
            </li>
          </ul>
          <br/>
          <h4>Skin Color</h4>
          <ul id="skin-colors" className="clearfix">
            <li>
              <a className="skin-changer" data-skin="" data-toggle="tooltip" title="Default" style={{backgroundColor: '#34495e'}}>
              </a>
            </li>
            <li>
              <a className="skin-changer" data-skin="theme-white" data-toggle="tooltip" title="White/Green" style={{backgroundColor: '#2ecc71'}}>
              </a>
            </li>
            <li>
              <a className="skin-changer blue-gradient" data-skin="theme-blue-gradient" data-toggle="tooltip" title="Gradient">
              </a>
            </li>
            <li>
              <a className="skin-changer" data-skin="theme-turquoise" data-toggle="tooltip" title="Green Sea" style={{backgroundColor: '#1abc9c'}}>
              </a>
            </li>
            <li>
              <a className="skin-changer" data-skin="theme-amethyst" data-toggle="tooltip" title="Amethyst" style={{backgroundColor: '#9b59b6'}}>
              </a>
            </li>
            <li>
              <a className="skin-changer" data-skin="theme-blue" data-toggle="tooltip" title="Blue" style={{backgroundColor: '#2980b9'}}>
              </a>
            </li>
            <li>
              <a className="skin-changer" data-skin="theme-red" data-toggle="tooltip" title="Red" style={{backgroundColor: '#e74c3c'}}>
              </a>
            </li>
            <li>
              <a className="skin-changer" data-skin="theme-whbl" data-toggle="tooltip" title="White/Blue" style={{backgroundColor: '#3498db'}}>
              </a>
            </li>
          </ul>
        </div>
      </div>
      // END MENU SIDEBAR
    )
  }
}

export default ConfigGeneral;
