import $ from 'jquery';
import Popper from 'popper.js';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import './NavBar.css';
//import Script from 'react-load-script'

class NavBar extends Component {
  componentDidMount = () => {
    window.jQuery = $;
    window.$ = $;
      let _this=this;
    	setTimeout(function() {
    		$('#content-wrapper > .row').css({
    			opacity: 1
    		});
    	}, 200);

    	$('#sidebar-nav,#nav-col-submenu').on('click', '.dropdown-toggle', function (e) {
    		e.preventDefault();

    		var $item = $(this).parent();

    		if (!$item.hasClass('open')) {
    			$item.parent().find('.open .submenu').slideUp('fast');
    			$item.parent().find('.open').toggleClass('open');
    		}

    		$item.toggleClass('open');

    		if ($item.hasClass('open')) {
    			$item.children('.submenu').slideDown('fast');
    		}
    		else {
    			$item.children('.submenu').slideUp('fast');
    		}
    	});

    	$('body').on('mouseenter', '#page-wrapper.nav-small #sidebar-nav .dropdown-toggle', function (e) {
    		if ($( document ).width() >= 992) {
    			var $item = $(this).parent();

    			if ($('body').hasClass('fixed-leftmenu')) {
    				var topPosition = $item.position().top;

    				if ((topPosition + 4*$(this).outerHeight()) >= $(window).height()) {
    					topPosition -= 6*$(this).outerHeight();
    				}

    				$('#nav-col-submenu').html($item.children('.submenu').clone());
    				$('#nav-col-submenu > .submenu').css({'top' : topPosition});
    			}

    			$item.addClass('open');
    			$item.children('.submenu').slideDown('fast');
    		}
    	});

    	$('body').on('mouseleave', '#page-wrapper.nav-small #sidebar-nav > .nav-pills > li', function (e) {
    		if ($( document ).width() >= 992) {
    			var $item = $(this);

    			if ($item.hasClass('open')) {
    				$item.find('.open .submenu').slideUp('fast');
    				$item.find('.open').removeClass('open');
    				$item.children('.submenu').slideUp('fast');
    			}

    			$item.removeClass('open');
    		}
    	});
    	$('body').on('mouseenter', '#page-wrapper.nav-small #sidebar-nav a:not(.dropdown-toggle)', function (e) {
    		if ($('body').hasClass('fixed-leftmenu')) {
    			$('#nav-col-submenu').html('');
    		}
    	});
    	$('body').on('mouseleave', '#page-wrapper.nav-small #nav-col', function (e) {
    		if ($('body').hasClass('fixed-leftmenu')) {
    			$('#nav-col-submenu').html('');
    		}
    	});

    	$('#make-small-nav').click(function (e) {
    		$('#page-wrapper').toggleClass('nav-small');
        if($('#tagMostrar').is(":visible")){
            $('#tagMostrar').hide();
        }else{
          $('#tagMostrar').show();
        }


    	});

    	// $(window).smartresize(function(){
    	// 	if ($( document ).width() <= 991) {
    	// 		$('#page-wrapper').removeClass('nav-small');
    	// 	}
    	// });

    	$('.mobile-search').click(function(e) {
    		e.preventDefault();

    		$('.mobile-search').addClass('active');
    		$('.mobile-search form input.form-control').focus();
    	});
    	$(document).mouseup(function (e) {
    		var container = $('.mobile-search');

    		if (!container.is(e.target) // if the target of the click isn't the container...
    			&& container.has(e.target).length === 0) // ... nor a descendant of the container
    		{
    			container.removeClass('active');
    		}
    	});

    	// $('.fixed-leftmenu #col-left').nanoScroller({
      // 	alwaysVisible: false,
      // 	iOSNativeScrolling: false,
      // 	preventPageScrolling: true,
      // 	contentClass: 'col-left-nano-content'
      // });

    	// build all tooltips from data-attributes
    	$("[data-toggle='tooltip']").each(function (index, el) {
    		$(el).tooltip({
    			placement: $(this).data("placement") || 'top'
    		});
    	});


    $.fn.removeClassPrefix = function(prefix) {
        this.each(function(i, el) {
            var classes = el.className.split(" ").filter(function(c) {
                return c.lastIndexOf(prefix, 0) !== 0;
            });
            el.className = classes.join(" ");
        });
        return this;
    };

  }

  render(){
    return(
      //MENU SIDEBAR
      <div id="nav-col">
        <section id="col-left" className="col-left-nano">
          <div id="col-left-inner" className="col-left-nano-content">
            {/*<div id="user-left-box" className="clearfix hidden-sm hidden-xs dropdown profile2-dropdown">
              <img alt="" src="img/samples/scarlet-159.png" />
              <div className="user-box">
                <span className="name">
                  <a href="#" className="dropdown-toggle" data-toggle="dropdown">
                    Scarlett J.
                    <i className="fa fa-angle-down"></i>
                  </a>
                  <ul className="dropdown-menu">
                    <li><a href="user-profile.html"><i className="fa fa-user"></i>Profile</a></li>
                    <li><a href="#"><i className="fa fa-cog"></i>Settings</a></li>
                    <li><a href="#"><i className="fa fa-envelope-o"></i>Messages</a></li>
                    <li><a href="#"><i className="fa fa-power-off"></i>Logout</a></li>
                  </ul>
                </span>
                <span className="status">
                  <i className="fa fa-circle"></i> Online
                </span>
              </div>
            </div>*/}
            <div className="collapse navbar-collapse navbar-ex1-collapse" id="sidebar-nav">
              <div style={{backgroundColor:'#2ECC71'}} className="hidden-xs hidden-sm">
                <a className="btn" id="make-small-nav" style={{height:'79px'}}>
                  <i className="fa fa-bars" style={{marginTop:'19px'}}> <span id="tagMostrar">Mostrar / ocultar</span></i>
                </a>
              </div>
              <ul className="nav nav-pills nav-stacked">
                {/*user options hidden*/}
                <li className="nav-header hidden-lg hidden-md">
                  user
                </li>
                <li className="hidden-lg hidden-md">
                  <a href="#" className="dropdown-toggle">
                    <i className="fa fa-edit"></i>
                    <span>User</span>
                    <i className="fa fa-angle-right drop-icon"></i>
                  </a>
                  <ul className="submenu">
                    <li><Link to="/Profile">Profile</Link></li>
                    <li><Link to="/Settings">Settings</Link></li>
                    {/*<li><a href="#">Messages</a></li>*/}
                    <li><Link to="/">Logout</Link></li>
                  </ul>
                </li>

                <li className={(this.props.activo==1) ? "active":""}>
                  <Link to="/Home">

                    <span>Inicio</span>
                    {/*<span className="label label-primary label-circle pull-right">28</span>*/}
                  </Link>
                </li>
                <li className="nav-header nav-header-first hidden-sm hidden-xs">
                  Gestion de usuarios
                </li>
                <li className={(this.props.activo==2) ? "active":""}>
                  <a href="#" className="dropdown-toggle">
                    <span>Gestion de usuarios</span>
                    <i className="fa fa-angle-right drop-icon"></i>
                  </a>
                  <ul className="submenu">
                    <li>
                      <Link to="/NewAdmin">
                        Administrativos
                      </Link>
                    </li>
                    <li>
                      <Link to="/Empleados">
                        Empleados
                      </Link>
                    </li>
                    <li>
                      <Link to="/Alumnos">
                        Alumnos
                      </Link>
                    </li>
                    <li>
                      <Link to="/Roles">
                        Roles
                      </Link>
                    </li>
                    <li>
                      <Link to="/Asistencia">
                        Asistencia
                      </Link>
                    </li>
                    <li>
                      <Link to="/Permisos">
                        Permisos
                      </Link>
                    </li>
                  </ul>
                </li>
                <li className="nav-header hidden-sm hidden-xs">
                  Administracion Escolar
                </li>
                <li className={(this.props.activo==3) ? "active":""}>
                  <a href="#" className="dropdown-toggle">

                    <span>Admon Escolar</span>
                    <i className="fa fa-angle-right drop-icon"></i>
                  </a>
                  <ul className="submenu">
                    <li>
                      <Link to="/Inscripcion">
                        Inscripcion
                      </Link>
                    </li>
                    <li>
                      <Link to="/Sedes">
                        Sedes
                      </Link>
                    </li>
                    <li>
                      <Link to="/Cursos">
                        Cursos
                      </Link>
                    </li>
                  </ul>
                </li>

                <li className="nav-header hidden-sm hidden-xs">
                  Administracion Financiera
                </li>
                <li className={(this.props.activo==4) ? "active":""}>
                  <a href="#" className="dropdown-toggle">

                    <span>Admon Financiera</span>
                    <i className="fa fa-angle-right drop-icon"></i>
                  </a>
                  <ul className="submenu">
                    <li>
                      <Link to="/RegPag">
                        Registrar Pago
                      </Link>
                    </li>
                    <li>
                      <Link to="/EstadoFin">
                        Estado financiero
                      </Link>
                    </li>
                  </ul>
                </li>
              </ul>
            </div>
          </div>
        </section>
        <div id="nav-col-submenu"></div>
      </div>
      // END MENU SIDEBAR

    )
  }
}

export default NavBar;
