import $ from 'jquery';
import Popper from 'popper.js';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link , Redirect} from "react-router-dom";

import {Dropdown,DropdownButton,ButtonToolbar,MenuItem} from 'react-bootstrap';


class CustomToggle extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(e) {
    e.preventDefault();

    this.props.onClick(e);
  }

  render() {
    return (
      <a href="" onClick={this.handleClick}>
        {this.props.children}
      </a>
    );
  }
}
class NavBar extends Component {
  constructor (props){
    super(props);
    this.state={
      auth:false,
      user:"none"
    }
  }
  componentDidMount = () => {
    //revisar la sesion
    if(sessionStorage.getItem('auth')=="true"){
      this.setState({ auth: true });
    }
  }

  flogout=(e)=>{
    e.preventDefault();
    sessionStorage.setItem('auth',false);
    this.setState({ auth: false });
    // console.log(this.state.auth);
  }
  render(){
    // console.log('pres');
    // console.log(localStorage.getItem('auth'));
    // console.log('pres');
    var redirectToReferrer = sessionStorage.getItem('auth');
    //redirigin a login si no hay una sesion
    if (redirectToReferrer == "false" || redirectToReferrer == null || redirectToReferrer == "null") {
      sessionStorage.setItem('auth',false);
      this.setState({ auth: false });
      return <Redirect to="/" />;
    }
    return(
      //MENU SIDEBAR
      <div id="content-header" className="clearfix">

          <div className="pull-left hidden-xs">
            <div className="xs-graph pull-left">
              <div className="graph-label">
                <img src="img/samples/user1.jpg" alt="" style={{
                 borderWidth:'2px',
                 borderColor:'#34495E',
                 alignItems:'center',
                 justifyContent:'center',
                 width:100,
                 height:100,
                 backgroundColor:'#fff',
                 borderRadius:100,
                 borderStyle:'solid'
               }}/>
              </div>
            </div>
            <div className="md-graph pull-left mrg-l-lg mrg-r-sm">
              <div className="graph-labelCust">
                <span className="hidden-xs">Bienvenido <b className=""> Nombre usuario</b></span>
              </div>
              <div className="graph-labelCustb">
                <span className="hidden-xs">Admin</span>
                <b className="fa fa-circle mrg-l-lg" style={{color:'gray'}}/>

                <Dropdown id="dropdown-custom-1" className="hidden-xs mrg-l-lg">
                <CustomToggle bsRole="toggle">view profile <b className="caret"></b></CustomToggle>
                <Dropdown.Menu className="super-colors">
                  <MenuItem eventKey="1" active>Perfil</MenuItem>
                  <MenuItem eventKey="2">opciones</MenuItem>
                  <MenuItem eventKey="3">
                    contraseña
                  </MenuItem>
                  <MenuItem divider />
                  <MenuItem onClick={(e)=>this.flogout(e)}>Logout</MenuItem>
                </Dropdown.Menu>
              </Dropdown>

              </div>
              <div className="graph-labelCust2">
                <span href="#">
                  {/*<i className="fa fa-envelope"></i>*/}
                  <img src="img/message2.png" alt="" />
                  <span className="count"><span>1</span></span>
                </span>
                <span href="#">
                  {/*<i className="fa fa-bell mrg-l-lg"></i>*/}
                  <img className="mrg-l-lg" src="img/campana2.png" alt="" />
                  <span className="count"><span>8</span></span>
                </span>
              </div>
            </div>
          </div>



        <div href="index.html" id="logo" className="navbar-brand pull-right hidden-xs">
          <img src="img/logo.png" alt="" className="normal-logo logo-white" style={{height:'120px',marginTop:'-40px'}}/>
          <img src="img/logo-black.png" alt="" className="normal-logo logo-black"/>
          <img src="img/logo-small.png" alt="" className="small-logo hidden-xs hidden-sm hidden"/>
        </div>
        <button className="navbar-toggle" data-target=".navbar-ex1-collapse" data-toggle="collapse" type="button">
					<span className="sr-only">Toggle navigation</span>
					<span className="fa fa-bars" style={{color:'gray'}}></span>
				</button>
      </div>
      // END MENU SIDEBAR
    )
  }
}

export default NavBar;
