import $ from 'jquery';
import Popper from 'popper.js';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import React, { Component } from 'react';
// import logo from './logo.svg';
import './App.css';
import {
  BrowserRouter as Router,
  Route,
  Link,
  Redirect,
  withRouter
} from "react-router-dom";

//pages import
import Login from './pages/Login';
import Home from './pages/Home';
//
import Empleados from './pages/Empleados';
import Alumnos from './pages/Alumnos';
import Roles from './pages/Roles';
import Asistencia from './pages/Asistencia';
//
import Inscripcion from './pages/Inscripcion';
import Sedes from './pages/Sedes';
import Cursos from './pages/Cursos';
//
import RegPag from './pages/RegPag';
import EstadoFin from './pages/EstadoFin';
import Permisos from './pages/Permisos';
import UserAdmin from './pages/usersAdmin';
//pages import



class App extends Component {

  render() {

    return (
      <Router>
       <div>
         <Route exact path="/Home" component={Home}/>
         <Route exact path="/" component={Login}/>
         <Route path="/Empleados" component={Empleados}/>
         <Route path="/Alumnos" component={Alumnos}/>

         <Route path="/Roles" component={Roles}/>
         <Route path="/Asistencia" component={Asistencia}/>
         <Route path="/Inscripcion" component={Inscripcion}/>
         <Route path="/Sedes" component={Sedes}/>
         <Route path="/Cursos" component={Cursos}/>
         <Route path="/RegPag" component={RegPag}/>
         <Route path="/EstadoFin" component={EstadoFin}/>
         <Route path="/Permisos" component={Permisos}/>
         <Route path="/NewAdmin" component={UserAdmin}/>
       </div>
      </Router>
    );
  }
}
const notpage=()=>{
  return(<div class="container center"><h3 class="info">Page not found</h3></div>);
}
export default App;
