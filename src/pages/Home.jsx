import $ from 'jquery';
import Popper from 'popper.js';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
//component import
import Navbar from '../component/NavBar';
import Header from '../component/Header';
import ConfigGeneral from '../component/ConfigGeneral';


class Home extends Component {
  componentDidMount() {

   }
  render (){
    return(
      <div>
        {/*<Header />cabecera*/}
        {/*<ConfigGeneral />menu de configuracion de tema*/}
        <div id="theme-wrapper">

    		<div id="page-wrapper" className="container">
    			<div className="row">
    			<Navbar activo={1}/>
    				<div id="content-wrapper">
    					<div className="row">
    						<div className="col-lg-12">
                  <div className="row">
    									<Header />
    							</div>
                  <h2 style={{color:'black'}}>Tus tareas Frecuentes</h2>
    							<div className="row">
                    <div className="col-lg-4 col-sm-6 col-xs-12">
                        <div className="main-box clearfix profile-box">
      										<div className="main-box-body clearfix">
      											<div className="profile-box-header" >
      												<img src="img/peoplehome.png" alt="" className="profile-imgCust img-responsive center-block" />
      												<h2>Visualizar empleados</h2>
      												<div className="job-position">
      													Actress
      												</div>
      											</div>

      											<div className="profile-box-footer clearfix">
    											</div>
    										</div>
    									</div>

    								</div>

                    <div className="col-lg-4 col-sm-6 col-xs-12">

                        <div className="main-box clearfix profile-box">
      										<div className="main-box-body clearfix">
      											<div className="profile-box-header" >
      												<img src="img/pesosHome.png" alt="" className="profile-imgCust img-responsive center-block" />
      												<h2>Registro de pagos</h2>
      												<div className="job-position">
      													Actress
      												</div>
      											</div>

      											<div className="profile-box-footer clearfix">
    											</div>
    										</div>
    									</div>

    								</div>

                    <div className="col-lg-4 col-sm-6 col-xs-12">

                        <div className="main-box clearfix profile-box">
      										<div className="main-box-body clearfix">
      											<div className="profile-box-header" >
      												<img src="img/checkHome.png" alt="" className="profile-imgCust img-responsive center-block" />
      												<h2>Inscripciones del mes</h2>
      												<div className="job-position">
      													Actress
      												</div>
      											</div>

      											<div className="profile-box-footer clearfix">
    											</div>
    										</div>
    									</div>

    								</div>


    							</div>


    							<div className="row">
    								<div className="col-lg-6 col-md-6 col-sm-6 col-xs-12">
    									<div className="main-box feed main-box-home">
    										<header className="main-box-header clearfix">
    											<h2 className="pull-left">Actividad reciente</h2>
    										</header>

    										<div className="main-box-body clearfix">
    											<ul>
    												<li className="list-group-item list-group-item-actividad clearfix">
                              <div className="img list-group-image">
    														<img src="img/pind-activity.png" alt=""/>
    													</div>
    													<div className="title">
    														<a href="#">juan montes</a> agregado a empleados
    													</div>
    													<div className="time-ago">
    														<i className="fa fa-clock-o"></i> 5 min.
    													</div>
    												</li>
    												<li className="list-group-item list-group-item-actividad clearfix">
                              <div className="img">
    														<img src="img/pind-activity.png" alt=""/>
    													</div>
    													<div className="title">
    														<a href="#">Adriana Gonzales</a> Agrego un sede
    													</div>
    													<div className="time-ago">
    														<i className="fa fa-clock-o"></i> 9 hours.
    													</div>
    												</li>

                            <li className="list-group-item list-group-item-actividad clearfix">
                              <div className="img">
    														<img src="img/pind-activity.png" alt=""/>
    													</div>
    													<div className="title">
    														<a href="#">martin hernandez</a> edito el grupo A
    													</div>
    													<div className="time-ago">
    														<i className="fa fa-clock-o"></i> 9 hours.
    													</div>
    												</li>

    											</ul>
    										</div>
    									</div>
    								</div>


    								<div className="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                      <div className="main-box main-box-home clearfix">
    										<header className="main-box-header clearfix">
    											<h2>Tareas de acceso rapido</h2>
    										</header>

    										<div className="main-box-body  clearfix">

                          <div className="list-group">
  														<Link to="/Alumnos" className="list-group-item list-group-item-bord">
    														<img src="img/personarapido.png" alt=""/>Registrar Alumno</Link>
  														<Link to="/RegPag" className="list-group-item list-group-item-bord">
                                <img src="img/cashrapido.png" alt=""/>Registrar Pago</Link>
  														<Link to="/Asistencia" className="list-group-item list-group-item-bord">
                                <img src="img/checkrapido.png" alt=""/>Pasar asistencia</Link>
  													</div>

    										</div>
    									</div>
    								</div>

    								<div className="col-lg-8 col-md-12 col-xs-12">

    								</div>
    							</div>

    						</div>
    					</div>

    					<footer id="footer-bar" className="row">
    						<p id="footer-copyright" className="col-xs-12">

    						</p>
    					</footer>
    				</div>
    			</div>
    		</div>
    	</div>
    </div>
    );
  };
}

export default Home;
