import $ from 'jquery';
import Popper from 'popper.js';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import './Empleados.css';
import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import {Modal,Button} from 'react-bootstrap';
import { Wizard, Steps, Step } from 'react-albus';
//component import
import Navbar from '../component/NavBar';
import Header from '../component/Header';
import ConfigGeneral from '../component/ConfigGeneral';
//component import

//Eliminar
class MyLargeDeleteModal extends React.Component {
  componentDidMount = () => {
    window.jQuery = $;
    window.$ = $;
    let _this=this;

  }
  render() {
    return (
      <Modal
        {...this.props}
        bsSize="large"
        aria-labelledby="contained-modal-title-lg"
      >
        <Modal.Header>
          <Modal.Title><strong>Eliminar</strong></Modal.Title>
        </Modal.Header>
        <Modal.Body style={{borderRadius:'20px'}}>
          <span>¿Esta seguro que desea al eliminar a este usuario?</span>
        </Modal.Body>
        <Modal.Footer>
          <div class="pull-right btn-group-sm">
            <Button onClick={this.props.onHide}>Close</Button>
            <Button onClick={this.props.onHide} bsStyle="danger">Eliminar</Button>
          </div>
        </Modal.Footer>
      </Modal>
    );
  }
}

//edit
class MyLargeEditModal extends React.Component {
  componentDidMount = () => {
    window.jQuery = $;
    window.$ = $;
    let _this=this;

  }
  render() {
    return (
      <Modal
        {...this.props}
        bsSize="large"
        aria-labelledby="contained-modal-title-lg"
      >
        <Modal.Body style={{borderRadius:'20px'}}>
          <div className="row">
            <div class="pull-right btn-group-sm">
              <button type="button" onClick={this.props.onHide} className="btn btn-info pull-right" style={{marginLeft:'10px',backgroundColor:'#34495e'}}>
                 Guardar
              </button>
              <button type="button" onClick={this.props.onHide} className="btn btn-danger pull-right" style={{marginLeft:'10px'}}>
                 Eliminar
              </button>
              <button type="button" onClick={this.props.onHide} className="btn btn-default pull-right">
                 Cerrar
              </button>
            </div>
            <div className="col-lg-4" style={{height: '650px',
                backgroundColor: 'rgba(0, 0, 0, 0.4)',
                marginTop: '-20px',
                marginLeft: '-10px',
                marginBottom: '-16px',
                borderRadius: '20px 0px 0 20px'}}>
              <div className="row">
                <h3 style={{color:'black',marginLeft:'15px',borderBottom:'none'}}>Informacion del Empleado</h3>
                <div className="graph-label" style={{marginLeft:'80px'}}>
                  <img src="img/samples/user1.jpg" alt="" style={{
                   borderWidth:'2px',
                   borderColor:'#34495E',
                   alignItems:'center',
                   justifyContent:'center',
                   width:100,
                   height:100,
                   backgroundColor:'#fff',
                   borderRadius:100,
                   borderStyle:'solid'
                 }}/>
                </div>
              </div>
              <br/>
              <ul id="progress" className="nodes">
                <li ><div class="node green"></div><p>Datos personales</p></li>
                <li ><div class="divider gray"></div></li>
                <li ><div class="node grey"></div><p>Direccion</p></li>
                <li ><div class="divider gray"></div></li>
                <li ><div class="node grey"></div><p>Datos bancarios</p></li>
                <li ><div class="divider gray"></div></li>
            </ul>

            </div>
            <div className="col-lg-7">
              <div id="myWizard" class="wizard">
                <Wizard>
                   <Steps>
                     <Step
                       id="merlin"
                       render={({ next }) => (
                         <div class="step-pane active" id="step1">
              							<br/>
              							<h4>Datos Personales</h4>
              							<div class="form-group">
              								<label class="control-label" for="inputSuccess">Nombre</label>
              								<input type="text" class="form-control" id="inputSuccess"/>

              							</div>
              							<div class="form-group">
              								<label class="control-label" for="inputWarning">Apellido paterno</label>
              								<input type="text" class="form-control" id="inputWarning"/>

              							</div>
              							<div class="form-group">
              								<label class="control-label" for="inputError">Apellido materno</label>
              								<input type="text" class="form-control" id="inputError"/>
              							</div>
                            <div class="form-group">
              								<label class="control-label" for="inputError">Fecha de nacimiento</label>
              							</div>
                            <div class="form-inline" role="form">
        												<div class="form-group">
        													<input type="number" class="form-control" id="day" placeholder="01" style={{width:'60px'}} />
        												</div>
        												<div class="form-group" style={{paddingLeft:'30px'}}>
        													<input type="number" class="form-control" id="month" placeholder="01" style={{width:'60px'}}/>
        												</div>
                                <div class="form-group" style={{marginLeft:'30px'}}>
        													<input type="number" class="form-control" id="year" placeholder="1900" style={{width:'100px'}}/>
        												</div>
        										</div>
                            <br/>
                            <div class="form-group">
              								<label class="control-label" for="inputError">Correo electronico</label>
              								<input type="email" class="form-control" id="inputError"/>
              							</div>
                            <div class="form-group">
              								<label class="control-label" for="inputError">telefono</label>
              								<input type="number" class="form-control" id="inputError"/>
              							</div>
                            <div class="actions btn-group-sm">
                              <button onClick={next} class="btn btn-success btn-mini btn-next" data-last="Finish">Siguiente<i class="icon-arrow-right"></i></button>
                            </div>
              						</div>
                       )}
                     />
                     <Step
                       id="gandalf"
                       render={({ next, previous }) => (
                         <div class="step-pane active" id="step1">
                           <br/>
                           <h4>Dirección</h4>
                           <div class="form-group">
                             <label class="control-label" for="inputSuccess">Direccion</label>
                             <input type="text" class="form-control" id="inputSuccess"/>

                           </div>
                           <div class="form-group">
                             <label class="control-label" for="inputWarning">Estado</label>
                             <input type="text" class="form-control" id="inputWarning"/>

                           </div>
                           <div class="form-group">
                             <label class="control-label" for="inputError">codigo postal</label>
                             <input type="text" class="form-control" id="inputError"/>
                           </div>
                           <div class="actions btn-group-sm">
                              <button onClick={previous} class="btn btn-default btn-mini btn-prev"> <i class="icon-arrow-left"></i>Anterior</button>
                              <button onClick={next} class="btn btn-success btn-mini btn-next" data-last="Finish">Siguiente<i class="icon-arrow-right"></i></button>
                          </div>
              					 </div>
                       )}
                     />
                     <Step
                       id="dumbledore2"
                       render={({ next, previous}) => (
                         <div class="step-pane" id="step3">
            							<br/>
            							<h4>Datos bancarios</h4>

            							<div class="form-group">
            								<label for="maskedDate">Date</label>
            								<div class="input-group">
            									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            									<input type="text" class="form-control" id="maskedDate"/>
            								</div>
            								<span class="help-block">ex. 99/99/9999</span>
            							</div>

            							<div class="form-group">
            								<label for="maskedPhone">Phone</label>
            								<div class="input-group">
            									<span class="input-group-addon"><i class="fa fa-phone"></i></span>
            									<input type="text" class="form-control" id="maskedPhone"/>
            								</div>
            								<span class="help-block">ex. (999) 999-9999</span>
            							</div>

            							<div class="form-group">
            								<label for="maskedPhoneExt">Phone + Ext</label>
            								<div class="input-group">
            									<span class="input-group-addon"><i class="fa fa-mobile-phone"></i></span>
            									<input type="text" class="form-control" id="maskedPhoneExt"/>
            								</div>
            								<span class="help-block">ex. (999) 999-9999? x99999</span>
            							</div>
                          <div class="actions btn-group-sm">
                             <button onClick={previous} class="btn btn-default btn-mini btn-prev"> <i class="icon-arrow-left"></i>Anterior</button>
                             <button onClick={next} class="btn btn-success btn-mini btn-next" data-last="Finish">Siguiente<i class="icon-arrow-right"></i></button>
                         </div>
            						</div>
                       )}
                     />
                     <Step
                       id="dumbledore4"
                       render={({ previous }) => (
                         <div class="step-pane" id="step3">
                           <br/>
               							<h4>Finalizar Registro</h4>

               							<div class="alert alert-info fade in" style={{margin: '100px 0'}}>
               								<i class="fa fa-check-circle fa-fw fa-lg"></i>
               								<strong>Ha terminado la captura</strong> pulse el boton guardar para terminar el registro.
               							</div>
                            <div class="actions btn-group-sm">
                               <button onClick={previous} class="btn btn-default btn-mini btn-prev"> <i class="icon-arrow-left"></i>Anterior</button>
                           </div>
                        </div>
                       )}
                     />
                   </Steps>
                 </Wizard>
                </div>
             </div>
           </div>
        </Modal.Body>
      </Modal>
    );
  }
}
//registro
class MyLargeModal extends React.Component {
  componentDidMount = () => {
    window.jQuery = $;
    window.$ = $;
    let _this=this;

  }
  render() {
    return (
      <Modal
        {...this.props}
        bsSize="large"
        aria-labelledby="contained-modal-title-lg"
      >
        <Modal.Header>

            <div class="pull-left ">
            <Modal.Title id="contained-modal-title-lg">Datos del empleado</Modal.Title>
            </div>
            <div class="pull-right btn-group-sm">
              <button type="button" onClick={this.props.onHide} className="btn btn-info pull-right" style={{marginLeft:'10px',backgroundColor:'#34495e'}}>
                 Guardar
              </button>
              <button type="button" onClick={this.props.onHide} className="btn btn-default pull-right">
                 Cerrar
              </button>
            </div>
        </Modal.Header>
        <Modal.Body>
          <div className="row">
            <div className="col-lg-4">
              <ul id="progress" className="nodes">
                <li ><div class="node green"></div><p>Datos personales</p></li>
                <li ><div class="divider white"></div></li>
                <li ><div class="node grey"></div><p>Direccion</p></li>
                <li ><div class="divider white"></div></li>
                <li ><div class="node grey"></div><p>Datos bancarios</p></li>
                <li ><div class="divider white"></div></li>
            </ul>

            </div>
            <div className="col-lg-7">
              <div id="myWizard" class="wizard">
                <Wizard>
                   <Steps>
                     <Step
                       id="merlin"
                       render={({ next }) => (
                         <div class="step-pane active" id="step1">
              							<br/>
              							<h4>Datos personales</h4>
              							<div class="form-group">
              								<label class="control-label" for="inputSuccess">Nombre</label>
              								<input type="text" class="form-control" id="inputSuccess"/>

              							</div>
              							<div class="form-group">
              								<label class="control-label" for="inputWarning">Apellido paterno</label>
              								<input type="text" class="form-control" id="inputWarning"/>

              							</div>
              							<div class="form-group">
              								<label class="control-label" for="inputError">Apellido materno</label>
              								<input type="text" class="form-control" id="inputError"/>
              							</div>
                            <div class="form-group">
              								<label class="control-label" for="inputError">Fecha de nacimiento</label>
              							</div>
                            <div class="form-inline" role="form">
        												<div class="form-group">
        													<input type="number" class="form-control" id="day" placeholder="01" style={{width:'60px'}} />
        												</div>
        												<div class="form-group" style={{paddingLeft:'30px'}}>
        													<input type="number" class="form-control" id="month" placeholder="01" style={{width:'60px'}}/>
        												</div>
                                <div class="form-group" style={{marginLeft:'30px'}}>
        													<input type="number" class="form-control" id="year" placeholder="1900" style={{width:'100px'}}/>
        												</div>
        										</div>
                            <br/>
                            <div class="form-group">
              								<label class="control-label" for="inputError">Correo electronico</label>
              								<input type="email" class="form-control" id="inputError"/>
              							</div>
                            <div class="form-group">
              								<label class="control-label" for="inputError">telefono</label>
              								<input type="number" class="form-control" id="inputError"/>
              							</div>
                            <div class="actions btn-group-sm">
                              <button onClick={next} class="btn btn-success btn-mini btn-next" data-last="Finish">Siguiente<i class="icon-arrow-right"></i></button>
                            </div>
              						</div>
                       )}
                     />
                     <Step
                       id="gandalf"
                       render={({ next, previous }) => (
                         <div class="step-pane active" id="step1">
                           <br/>
                           <h4>Direccion</h4>
                           <div class="form-group">
                             <label class="control-label" for="inputSuccess">Direccion</label>
                             <input type="text" class="form-control" id="inputSuccess"/>

                           </div>
                           <div class="form-group">
                             <label class="control-label" for="inputWarning">Estado</label>
                             <input type="text" class="form-control" id="inputWarning"/>

                           </div>
                           <div class="form-group">
                             <label class="control-label" for="inputError">codigo postal</label>
                             <input type="text" class="form-control" id="inputError"/>
                           </div>
                           <div class="actions btn-group-sm">
                              <button onClick={previous} class="btn btn-default btn-mini btn-prev"> <i class="icon-arrow-left"></i>Anterior</button>
                              <button onClick={next} class="btn btn-success btn-mini btn-next" data-last="Finish">Siguiente<i class="icon-arrow-right"></i></button>
                          </div>
              					 </div>
                       )}
                     />
                     <Step
                       id="dumbledore2"
                       render={({ next, previous}) => (
                         <div class="step-pane" id="step3">
            							<br/>
            							<h4>Datos bancarios</h4>

            							<div class="form-group">
            								<label for="maskedDate">Date</label>
            								<div class="input-group">
            									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            									<input type="text" class="form-control" id="maskedDate"/>
            								</div>
            								<span class="help-block">ex. 99/99/9999</span>
            							</div>

            							<div class="form-group">
            								<label for="maskedPhone">Phone</label>
            								<div class="input-group">
            									<span class="input-group-addon"><i class="fa fa-phone"></i></span>
            									<input type="text" class="form-control" id="maskedPhone"/>
            								</div>
            								<span class="help-block">ex. (999) 999-9999</span>
            							</div>

            							<div class="form-group">
            								<label for="maskedPhoneExt">Phone + Ext</label>
            								<div class="input-group">
            									<span class="input-group-addon"><i class="fa fa-mobile-phone"></i></span>
            									<input type="text" class="form-control" id="maskedPhoneExt"/>
            								</div>
            								<span class="help-block">ex. (999) 999-9999? x99999</span>
            							</div>
                          <div class="actions btn-group-sm">
                             <button onClick={previous} class="btn btn-default btn-mini btn-prev"> <i class="icon-arrow-left"></i>Anterior</button>
                             <button onClick={next} class="btn btn-success btn-mini btn-next" data-last="Finish">Siguiente<i class="icon-arrow-right"></i></button>
                         </div>
            						</div>
                       )}
                     />
                     <Step
                       id="dumbledore4"
                       render={({ previous }) => (
                         <div class="step-pane" id="step3">
                           <br/>
               							<h4>Finalizar Registro</h4>

               							<div class="alert alert-info fade in" style={{margin: '100px 0'}}>
               								<i class="fa fa-check-circle fa-fw fa-lg"></i>
               								<strong>Ha terminado la captura</strong> pulse el boton guardar para terminar el registro.
               							</div>
                            <div class="actions btn-group-sm">
                               <button onClick={previous} class="btn btn-default btn-mini btn-prev"> <i class="icon-arrow-left"></i>Anterior</button>
                           </div>
                        </div>
                       )}
                     />
                   </Steps>
                 </Wizard>
                </div>
             </div>
           </div>
        </Modal.Body>
        <Modal.Footer>

        </Modal.Footer>
      </Modal>
    );
  }
}
class Cursos extends Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      lgShow: false,
      lgEditShow : false,
      lgdelShow : false
    };
  }
  render (){
    let lgClose = () => this.setState({ lgShow: false });
    let lgEditClose = () => this.setState({ lgEditShow: false });
    let lgDelClose = () => this.setState({ lgdelShow: false });

    return(
      <div>

        {/*<ConfigGeneral />menu de configuracion de tema*/}
        <div id="theme-wrapper">

    		<div id="page-wrapper" className="container">
    			<div className="row">
    			<Navbar activo={1}/>
    				<div id="content-wrapper">
    					<div className="row">
    						<div className="col-lg-12">
    							<div className="row">
    									<Header />
    							</div>
                  <div className="row">
                      <div className="pull-left hidden-xs">
  											<div className="xs-graph pull-left">
  												<div className="graph-label">
  													<img  src="img/pople.png" style={{height:'60px'}} alt=""/>
  												</div>
  											</div>
                        <div className="md-graph pull-left mrg-l-lg mrg-r-sm">
  												<div className="graph-labelCustc">
  													<b style={{fontWeight:300}}>Gestion de usuarios</b>
  												</div>
  											</div>
  											<div className="md-graph pull-left mrg-l-lg mrg-r-sm">
  												<div className="graph-label">
  													<b>|</b> Cursos
  												</div>
  											</div>
  										</div>
                      <div class="pull-right top-page-ui btn-group">
  											<div onClick={() => this.setState({ lgShow: true })} className="pull-right" style={{borderColor:'transparent',backgroundColor:'transparent',display:'flex'}}>
                            <button style={{
                                width: '40px',
                                height: '40px',
                                borderRadius: '5px 0px 0px 5px',
                                backgroundColor: '#2ecc71'
                              }}><i class="fa fa-plus-circle fa-xs"></i></button>
                            <button style={{
                                width: '169px',
                                height: '40px',
                                borderRadius:'0px 5px 5px 0',
                                backgroundColor: '#34495e',
                                fontSize:'16px',
                                textAlign: 'center',
                                verticalAlign: 'middle',
                                lineheight:'40px !important',
                                color:'white'
                              }}>Añadir empleado</button>
  											</div>
  										</div>
                  </div>
    							<div className="row">
                    <div className="col-lg-8 col-md-8 col-xs-8">
                        <div className="main-box-body clearfix">
                          <div className="table-responsive">
                            <table className="table user-list table-hover">
                              <thead>
                                <tr>
                                  <th><span>Nombre</span></th>
                                  <th><span>Apellido paterno</span></th>
                                  <th><span>Apellido materno</span></th>
                                  <th className="text-center" style={{borderLeft:' 1px solid #000'}}><span>Puesto</span></th>
                                  <th className="text-center"><span>Asistencia</span></th>
                                  <th>&nbsp;</th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>
                                    <span>ISAAC NICOLAS</span>
                                  </td>
                                  <td>
                                    RAMIREZ
                                  </td>
                                  <td className="text-center">
                                    GONZALEZ
                                  </td>
                                  <td style={{borderLeft:' 1px solid #000'}}>
                                    <span className="user-subhead">Admin</span>
                                  </td>
                                  <td style={{width: '20%'}}>
                                    <div className="form-group">
                                        <div className="checkbox-nice checkbox-inline">
                                          <input type="checkbox" id="checkbox-inl-1"/>
                                          <label for="checkbox-inl-1"></label>
                                        </div>
                                      </div>
                                  </td>
                                  <td>
                                    <span onClick={() => this.setState({ lgdelShow: true })} class="fa-stack" style={{borderRadius:'4px',backgroundColor:'red'}}>
																		<i class="fa fa-trash-o fa-stack-1x"></i>
																	</span>
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <span>JUAN MANUEL</span>
                                  </td>
                                  <td>
                                    CORTES
                                  </td>
                                  <td className="text-center">
                                    PAREDES
                                  </td>
                                  <td style={{borderLeft:' 1px solid #000'}}>
                                    <span className="user-subhead">Admin</span>
                                  </td>
                                  <td style={{width: '20%'}}>
                                    <div className="form-group">
                                        <div className="checkbox-nice checkbox-inline">
                                          <input type="checkbox" id="checkbox-inl-1"/>
                                          <label for="checkbox-inl-1"></label>
                                        </div>
                                      </div>
                                  </td>
                                  <td>
                                    <span onClick={() => this.setState({ lgdelShow: true })} class="fa-stack" style={{borderRadius:'4px',backgroundColor:'red'}}>
																		<i class="fa fa-trash-o fa-stack-1x"></i>
																	</span>
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <span>ANDRES HIPOLITO</span>
                                  </td>
                                  <td>
                                    GUTIERREZ
                                  </td>
                                  <td className="text-center">
                                    GUTIERREZ
                                  </td>
                                  <td style={{borderLeft:' 1px solid #000'}}>
                                    <span className="user-subhead">Admin</span>
                                  </td>
                                  <td style={{width: '20%'}}>
                                    <div className="form-group">
                                        <div className="checkbox-nice checkbox-inline">
                                          <input type="checkbox" id="checkbox-inl-1"/>
                                          <label for="checkbox-inl-1"></label>
                                        </div>
                                      </div>
                                  </td>
                                  <td>
                                    <span onClick={() => this.setState({ lgdelShow: true })} class="fa-stack" style={{borderRadius:'4px',backgroundColor:'red'}}>
																		<i class="fa fa-trash-o fa-stack-1x"></i>
																	</span>
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <span>MAGALY</span>
                                  </td>
                                  <td>
                                    ALBA
                                  </td>
                                  <td className="text-center">
                                    MARTINEZ
                                  </td>
                                  <td style={{borderLeft:' 1px solid #000'}}>
                                    <span className="user-subhead">Admin</span>
                                  </td>
                                  <td style={{width: '20%'}}>
                                    <div className="form-group">
                                        <div className="checkbox-nice checkbox-inline">
                                          <input type="checkbox" id="checkbox-inl-1"/>
                                          <label for="checkbox-inl-1"></label>
                                        </div>
                                      </div>
                                  </td>
                                  <td>
                                    <span onClick={() => this.setState({ lgdelShow: true })} class="fa-stack" style={{borderRadius:'4px',backgroundColor:'red'}}>
																		<i class="fa fa-trash-o fa-stack-1x"></i>
																	</span>
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <span>MADIAN</span>
                                  </td>
                                  <td>
                                    FLORES
                                  </td>
                                  <td className="text-center">
                                    CÁRDENAS
                                  </td>
                                  <td style={{borderLeft:' 1px solid #000'}}>
                                    <span className="user-subhead">Admin</span>
                                  </td>
                                  <td style={{width: '20%'}}>
                                    <div className="form-group">
                                        <div className="checkbox-nice checkbox-inline">
                                          <input type="checkbox" id="checkbox-inl-1"/>
                                          <label for="checkbox-inl-1"></label>
                                        </div>
                                      </div>
                                  </td>
                                  <td>
                                    <span onClick={() => this.setState({ lgdelShow: true })} class="fa-stack" style={{borderRadius:'4px',backgroundColor:'red'}}>
																		<i class="fa fa-trash-o fa-stack-1x"></i>
																	</span>
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <span>REGINA</span>
                                  </td>
                                  <td>
                                    GARCÍA
                                  </td>
                                  <td className="text-center">
                                    VALERIO
                                  </td>
                                  <td style={{borderLeft:' 1px solid #000'}}>
                                    <span className="user-subhead">Admin</span>
                                  </td>
                                  <td style={{width: '20%'}}>
                                    <div className="form-group">
                                        <div className="checkbox-nice checkbox-inline">
                                          <input type="checkbox" id="checkbox-inl-1"/>
                                          <label for="checkbox-inl-1"></label>
                                        </div>
                                      </div>
                                  </td>
                                  <td>
                                    <span onClick={() => this.setState({ lgdelShow: true })} class="fa-stack" style={{borderRadius:'4px',backgroundColor:'red'}}>
																		<i class="fa fa-trash-o fa-stack-1x"></i>
																	</span>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                    </div>

                      <div className="col-lg-4 col-md-4 col-sm-4">
                      	<div className="main-box clearfix" style={{backgroundColor:'#d8d8d8'}}>
                      		<header className="main-box-header clearfix">
                            <br/>
                              <div className="pull-left hidden-xs">
                                <div className="graph-label">
                                  <img src="img/samples/user1.jpg" alt="" style={{
                                   borderWidth:'2px',
                                   borderColor:'#34495E',
                                   alignItems:'center',
                                   justifyContent:'center',
                                   width:80,
                                   height:80,
                                   backgroundColor:'#fff',
                                   borderRadius:100,
                                   borderStyle:'solid'
                                 }}/>
                                </div>
                                <div className="profile-stars" style={{width:'250px'}}>
                                  <h2>Aandres Hipolito Gutierrez Gutierrez</h2>
                                </div>

                              </div>
                              <div class="pull-right top-page-ui btn-group">
                                <div onClick={() => this.setState({ lgEditShow: true })} className="pull-right" style={{borderColor:'transparent',backgroundColor:'transparent',display:'flex'}}>
                                    <button style={{
                                        width: '80',
                                        height: '40px',
                                        borderRadius:'6px',
                                        backgroundColor: '#34495e',
                                        fontSize:'14px',
                                        textAlign: 'center',
                                        verticalAlign: 'middle',
                                        lineheight:'40px !important',
                                        color:'white',
                                        marginRight:'15px'
                                      }}>Editar</button>
                                </div>
                              </div>
                      		</header>

                      		<div className="main-box-body clearfix">

                      			<div className="profile-stars">
                      				<strong>Puesto: </strong><span>Profesor</span>
                      			</div>
                            <br/>
                            <div className="profile-since">
                      				<strong>cumpleaños: </strong><span>23/mayo</span>
                      			</div>
                            <br/>
                            <div className="profile-stars">
                      				<strong>correo electrónico: </strong><span>hipolitoGt@ipn.com</span>
                      			</div>
                            <br/>
                            <div className="profile-stars">
                      				<strong>Direccion: </strong><span>Av.madero , colonia San Rafael ,Ciudad de mexico, codigo postal:54930</span>
                      			</div>
                            <br/>
                      			<div className="profile-details">
                      				<ul className="fa-ul">
                      					<li><i className="fa-li fa fa-phone"></i>Telefono 1: <span>456 4312 12</span></li>
                      				</ul>
                      			</div>
                      		</div>

                      	</div>
                      </div>

    							</div>
    						</div>
    					</div>
              <MyLargeModal show={this.state.lgShow} onHide={lgClose} />
              <MyLargeEditModal show={this.state.lgEditShow} onHide={lgEditClose} />
              <MyLargeDeleteModal show={this.state.lgdelShow} onHide={lgDelClose} />

    					<footer id="footer-bar" className="row">
    						<p id="footer-copyright" className="col-xs-12">

    						</p>
    					</footer>
    				</div>
    			</div>
    		</div>
    	</div>
    </div>
    );
  };
}

export default Cursos;
