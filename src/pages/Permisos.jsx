import $ from 'jquery';
import Popper from 'popper.js';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import './Permisos.css';
import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import {Modal,Button} from 'react-bootstrap';
import { Wizard, Steps, Step } from 'react-albus';
import { urlCourses, urlPeople}  from './Globals';
//component import
import Navbar from '../component/NavBar';
import Header from '../component/Header';
import ConfigGeneral from '../component/ConfigGeneral';
//component import
const permisosC={
	"success": true,
	"permissions": []
}



//Eliminar
class MyLargeDeleteModal extends React.Component {
  constructor(props){
    super(props);
  }
  componentDidMount = () => {
    window.jQuery = $;
    window.$ = $;
    let _this=this;

  }
  render() {
    return (
      <Modal
        {...this.props}
        bsSize="large"
        aria-labelledby="contained-modal-title-lg"
      >
        <Modal.Header>
          <Modal.Title className="titleModalR"><strong>Eliminar</strong></Modal.Title>
        </Modal.Header>
        <Modal.Body style={{borderRadius:'20px'}}>
          <span>¿Esta seguro que desea al eliminar a este permiso?</span>
        </Modal.Body>
        <Modal.Footer>
          <div class="pull-right btn-group-sm">
            <Button onClick={ ()=> this.props.showModalDelete(false) }>Close</Button>
            <Button onClick={()=>{
              fetch(urlPeople + "/api/permissions/" + this.props.dataRow.permissionId.S, {
                method: 'DELETE',
                headers: {
                  Accept: 'application/json',
                  'Content-Type': 'application/json',
                },
                // body: JSON.stringify({
                //   name: this.state.inputName,
                //   description: this.state.inputDescription,
                //   typePermission: 7
                // }),
              })
              .then((response) => response.json())
              .then((responseJson) => {
                this.props.updateList()
                this.props.showModalDelete(false)
                // this.props.onHide()
              })
              .catch((error) => {
                console.error(error);
                // this.props.onHide()
                this.props.showModalDelete(false)
                alert(error);
              });
            }} bsStyle="danger">Eliminar</Button>
          </div>
        </Modal.Footer>
      </Modal>
    );
  }
}



//editar un permiso
class MyLargeEditModal extends React.Component {

  constructor(props){
    super(props);
  }
  componentDidMount = () => {
    window.jQuery = $;
    window.$ = $;
    let _this=this;

    this.state = {}
  }
  render() {
    return (
      <Modal
        {...this.props}
        bsSize="large"
        aria-labelledby="contained-modal-title-lg"
      >
        <Modal.Header>

            <div class="pull-left ">
            <Modal.Title id="contained-modal-title-lg" className="titleModalR"> Editar Permisos</Modal.Title>
            </div>
            <div class="pull-right btn-group-sm">
              <button type="button" onClick={this.props.onHide} className="btn btn-info pull-right" style={{marginLeft:'10px',backgroundColor:'#34495e'}}>
                 Guardar
              </button>
              <button type="button" onClick={() => this.props.showModalEdit(false)} className="btn btn-default pull-right">
                 Cerrar
              </button>
            </div>
        </Modal.Header>
        <Modal.Body>
          <div className="row">
            {/* <div className="col-lg-4">
              <ul id="progress" className="nodes">
                <li ><div class="node green"></div><p>Permiso</p></li>
                <li ><div class="divider white"></div></li>
                <li ><div class="node grey"></div><p>Acciones</p></li>
                <li ><div class="divider white"></div></li>
            </ul> */}

            {/* </div> */}
            <div className="col-lg-7">
              <div id="myWizard" class="wizard">
                <Wizard>
                   <Steps>
                     <Step
                       id="merlin"
                       render={({ next }) => (
                         <div class="step-pane active" id="step1">
              							<br/>
              							<h4>Edita el nombre o la descripción</h4>
              							<div class="form-group">
              								<label class="control-label" for="inputSuccess">Nombre</label>
              								<input type="text" class="form-control" id="inputSuccess"/>
              							</div>
                            <div class="form-group">
                              <label>Activo</label>
                              <select class="form-control">
                                <option>Activo</option>
                                <option>Inactivo</option>
                              </select>
                            </div>

                            <div class="actions btn-group-sm">
                              <button onClick={next} class="btn btn-success btn-mini btn-next" data-last="Finish">Siguiente<i class="icon-arrow-right"></i></button>
                            </div>
              						</div>
                       )}
                     />
                     <Step
                       id="gandalf"
                       render={({ next, previous }) => (
                         <div class="step-pane active" id="step1">
                           <br/>
                           <h4>Acciones relacionadas</h4>
                           <div class="form-group">
                             <div class="form-group col-xs-3">
                               <label class="switch">Account manager</label>
                               <label class="switch">
                                 <input type="checkbox"/>
                                 <span class="slider round"></span>
                               </label>
                             </div>
                              <div class="form-group col-xs-3">
                                <label class="switch">Account manager</label>
                                <label class="switch">
                                  <input type="checkbox"/>
                                  <span class="slider round"></span>
                                </label>
     													</div>
                              <div class="form-group col-xs-3">
                                <label class="switch">Account manager</label>
                                <label class="switch">
                                  <input type="checkbox" defaultChecked/>
                                  <span class="slider round"></span>
                                </label>
     													</div>
     													<div class="form-group col-xs-3">
                                <label class="switch">Account manager</label>
                                <label class="switch">
                                  <input type="checkbox" defaultChecked/>
                                  <span class="slider round"></span>
                                </label>
     													</div>
               							</div>
                            <div class="form-group">
                              <div class="form-group col-xs-3">
                                <label class="switch">Account manager</label>
                                <label class="switch">
                                  <input type="checkbox"/>
                                  <span class="slider round"></span>
                                </label>
                              </div>
                               <div class="form-group col-xs-3">
                                 <label class="switch">manager</label>
                                 <label class="switch">
                                   <input type="checkbox"/>
                                   <span class="slider round"></span>
                                 </label>
      													</div>
                               <div class="form-group col-xs-3">
                                 <label class="switch">edit courses</label>
                                 <label class="switch">
                                   <input type="checkbox" defaultChecked/>
                                   <span class="slider round"></span>
                                 </label>
      													</div>
      													<div class="form-group col-xs-3">
                                 <label class="switch">Account manager</label>
                                 <label class="switch">
                                   <input type="checkbox"/>
                                   <span class="slider round"></span>
                                 </label>
      													</div>
                							</div>

                           <div class="actions btn-group-sm col-xs-12">
                              <button onClick={previous} class="btn btn-default btn-mini btn-prev"> <i class="icon-arrow-left"></i>Anterior</button>
                              <button onClick={next} class="btn btn-success btn-mini btn-next" data-last="Finish">Siguiente<i class="icon-arrow-right"></i></button>
                          </div>
              					 </div>
                       )}
                     />
                     <Step
                       id="dumbledore4"
                       render={({ previous }) => (
                         <div class="step-pane" id="step3">
                           <br/>
               							<h4>Finalizar Registro</h4>

               							<div class="alert alert-info fade in" style={{margin: '100px 0'}}>
               								<i class="fa fa-check-circle fa-fw fa-lg"></i>
               								<strong>Ha terminado la captura</strong> pulse el boton guardar para terminar el registro.
               							</div>
                            <div class="actions btn-group-sm">
                               <button onClick={previous} class="btn btn-default btn-mini btn-prev"> <i class="icon-arrow-left"></i>Anterior</button>
                           </div>
                        </div>
                       )}
                     />
                   </Steps>
                 </Wizard>
                </div>
             </div>
           </div>
        </Modal.Body>
        <Modal.Footer>

        </Modal.Footer>
      </Modal>
    );
  }
}



// Registro de Nuevos permisos
class MyLargeModal extends React.Component {


  constructor(props) {
    super(props);

    this.state = {
      lgShow: false,
      lgEditShow : false,
      lgdelShow : false,
      inputName : '',
      inputDescription: '',
      // data:permisosC,
    };

    // this.changeName = this.changeName.bind(this);
    // this.changeDescription = this
    // this.handleSubmit = this.handleSubmit.bind(this);
  }


  componentDidMount(){
    window.jQuery = $;
    window.$ = $;
    let _this=this;
  }

  changeName(event){
    this.setState({inputName: event.target.value});
  }
  changeDescription(event){
    this.setState({inputDescription: event.target.value});
  }


  render() {
    return (
      <Modal
        {...this.props}
        bsSize="large"
        aria-labelledby="contained-modal-title-lg"
      >
        <Modal.Header>

            <div class="pull-left ">
            <Modal.Title id="contained-modal-title-lg" className="titleModalR">Permisos</Modal.Title>
            </div>
            <div class="pull-right btn-group-sm">
              <button type="button"
                onClick= {()=>{
                  fetch(urlPeople + "/api/permissions", {
                    method: 'POST',
                    headers: {
                      Accept: 'application/json',
                      'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                      name: this.state.inputName,
                      description: this.state.inputDescription,
                      typePermission: 7
                    }),
                  })
                  .then((response) => response.json())
                  .then((responseJson) => {
                    this.props.updateList()
                    this.props.onHide()
                  })
                  .catch((error) => {
                    console.error(error);
                    this.props.onHide()
                    alert(error);
                  });
                }}
                className="btn btn-info pull-right"
                style={{marginLeft:'10px',
                backgroundColor:'#34495e'}}>
                 Guardar
              </button>
              <button type="button" onClick={this.props.onHide} className="btn btn-default pull-right">
                 Cerrar
              </button>
            </div>
        </Modal.Header>
        <Modal.Body>
          <div className="row">
            <div className="col-lg-4">
              {/* <ul id="progress" className="nodes">
                <li ><div class="node green"></div><p>Nuevo permiso</p></li>
                <li ><div class="divider white"></div></li>
                <li ><div class="node grey"></div><p>Acciones</p></li>
                <li ><div class="divider white"></div></li>
              </ul> */}

            </div>
            <div className="col-lg-7">
              <div id="myWizard" class="wizard">
                <Wizard>
                   <Steps>
                     <Step
                       id="merlin"
                       render={({ next }) => (
                         <div class="step-pane active" id="step1">
              							<br/>
              							<h4>Ingresa el nombre y la descripción del nuevo permiso</h4>
              							<div class="form-group">
              								<label class="control-label" for="inputSuccess">Nombre</label>
              								<input type="text" class="form-control" id="inputName" value={this.state.inputName} onChange={ this.changeName.bind(this) }/>
              							</div>
                            <div class="form-group">
              								<label class="control-label" for="inputSuccess">Descripción</label>
              								<input type="text" class="form-control" id="inputDescription"  value={this.state.inputDescription}  onChange={ this.changeDescription.bind(this) }/>
              							</div>

                            {/* <div class="form-group">
                              <label>Activo</label>
                              <select class="form-control">
                                <option>Activo</option>
                                <option>Inactivo</option>
                              </select>
                            </div> */}

                            {/* <div class="actions btn-group-sm">
                              <button onClick={next} class="btn btn-success btn-mini btn-next" data-last="Finish">Siguiente<i class="icon-arrow-right"></i></button>
                            </div> */}
              						</div>
                       )}
                     />
                     <Step
                       id="gandalf"
                       render={({ next, previous }) => (
                         <div class="step-pane active" id="step1">
                           <br/>
                           <h4>Acciones relacionadas</h4>
                           <div class="form-group">
                             <div class="form-group col-xs-3">
                               <label class="switch">Account manager</label>
                               <label class="switch">
                                 <input type="checkbox"/>
                                 <span class="slider round"></span>
                               </label>
                             </div>
                              <div class="form-group col-xs-3">
                                <label class="switch">Account manager</label>
                                <label class="switch">
                                  <input type="checkbox"/>
                                  <span class="slider round"></span>
                                </label>
     													</div>
                              <div class="form-group col-xs-3">
                                <label class="switch">Account manager</label>
                                <label class="switch">
                                  <input type="checkbox" defaultChecked/>
                                  <span class="slider round"></span>
                                </label>
     													</div>
     													<div class="form-group col-xs-3">
                                <label class="switch">Account manager</label>
                                <label class="switch">
                                  <input type="checkbox" defaultChecked/>
                                  <span class="slider round"></span>
                                </label>
     													</div>
               							</div>
                            <div class="form-group">
                              <div class="form-group col-xs-3">
                                <label class="switch">Account manager</label>
                                <label class="switch">
                                  <input type="checkbox"/>
                                  <span class="slider round"></span>
                                </label>
                              </div>
                               <div class="form-group col-xs-3">
                                 <label class="switch">manager</label>
                                 <label class="switch">
                                   <input type="checkbox"/>
                                   <span class="slider round"></span>
                                 </label>
      													</div>
                               <div class="form-group col-xs-3">
                                 <label class="switch">edit courses</label>
                                 <label class="switch">
                                   <input type="checkbox" defaultChecked/>
                                   <span class="slider round"></span>
                                 </label>
      													</div>
      													<div class="form-group col-xs-3">
                                 <label class="switch">Account manager</label>
                                 <label class="switch">
                                   <input type="checkbox"/>
                                   <span class="slider round"></span>
                                 </label>
      													</div>
                							</div>

                           <div class="actions btn-group-sm col-xs-12">
                              <button onClick={previous} class="btn btn-default btn-mini btn-prev"> <i class="icon-arrow-left"></i>Anterior</button>
                              <button onClick={next} class="btn btn-success btn-mini btn-next" data-last="Finish">Siguiente<i class="icon-arrow-right"></i></button>
                          </div>
              					 </div>
                       )}
                     />

                     <Step
                       id="dumbledore4"
                       render={({ previous }) => (
                         <div class="step-pane" id="step3">
                           <br/>
               							<h4>Finalizar Registro</h4>

               							<div class="alert alert-info fade in" style={{margin: '100px 0'}}>
               								<i class="fa fa-check-circle fa-fw fa-lg"></i>
               								<strong>Ha terminado la captura</strong> pulse el boton guardar para terminar el registro.
               							</div>
                            <div class="actions btn-group-sm">
                               <button onClick={previous} class="btn btn-default btn-mini btn-prev"> <i class="icon-arrow-left"></i>Anterior</button>
                           </div>
                        </div>
                       )}
                     />
                   </Steps>
                 </Wizard>
                </div>
             </div>
           </div>
        </Modal.Body>
        <Modal.Footer>

        </Modal.Footer>
      </Modal>
    );
  }
}

class HeaderCols extends React.Component {
  render() {
    return (<tr>
              <th><span>Permiso</span></th>
              <th style={{borderLeft:' 1px solid #000'}}><span>activo</span></th>
              <th>Editar</th>
              <th>Eliminar</th>
            </tr>)
  }
}



// rows
class RowsRols extends React.Component {
  constructor(props){
    super(props)
  }

  // columnasRend=(nombre, activo, index)=>{
  //     return(
  //       <td>
  //         <div key={"rowdivUp"+index} className="form-group text-center">
  //             <div key={"rowdivin"+index} className="checkbox-nice checkbox-inline">
  //               <input key={"checkin"+index} type="checkbox" id={index}/>
  //               <label key={"labin"+index} htmlFor={index}></label>
  //             </div>
  //           </div>
  //       </td>)
  // }

  render() {
    const permisosData=this.props.data
    //console.log(permisosData)
    //console.log(this.props.rowI)
    return (

      <tr>
        <td>
          <span>{permisosData.name.S}</span>
        </td>
        <td style={{borderLeft:' 1px solid #000'}}>
          <div  className="checkbox-nice checkbox-inline">
            <input  type="checkbox" id={"cast"+this.props.rowI}/>
            <label  htmlFor={"cast"+this.props.rowI}></label>
          </div>
        </td>
        <td>
          {/* <span onClick={() => this.setState({ lgEditShow: true })}  class="fa-stack" style={{borderRadius:'4px',backgroundColor:'blue'}}>
            <i class="fa fa-pencil fa-stack-1x"></i>
          </span> */}
          <span onClick={() => this.props.showModalEdit(true)  }  class="fa-stack" style={{borderRadius:'4px',backgroundColor:'blue'}}>
            <i class="fa fa-pencil fa-stack-1x"></i>
          </span>
        </td>
        <td>
          <span onClick={() => this.props.showModalDelete(true, this.props.data) }  class="fa-stack" style={{borderRadius:'4px',backgroundColor:'red'}}>
          {/* <span onClick={() => alert(JSON.stringify(this.props.data)) }  class="fa-stack" style={{borderRadius:'4px',backgroundColor:'red'}}>  */}
            <i class="fa fa-trash-o fa-stack-1x"></i>
          </span>
        </td>
      </tr>
    );
  }
}



// tabla
class Drawtable extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};
  }
  async componentDidMount(){

  }
  //metodos render
  renderRows(data, cols, index) {
    return <RowsRols data={data} columnas={cols} rowI={index} showModalEdit={this.props.showModalEdit} showModalDelete={this.props.showModalDelete}/>;
  }
  renderHeads() {
    return <HeaderCols/>;
  }

  render() {
    const permisosTit=this.props.data.permissions;

    return (
      <table className="table user-list table-hover">
        <thead>
            { this.renderHeads()}

        </thead>
        <tbody>
          {permisosTit.map((data, index) => {
            return this.renderRows(data, permisosTit.length, index);
          })}
        </tbody>
      </table>
    );
  }
}








// clase principal

export default class Permisos extends Component {
  constructor(props) {
    super(props);

    this.state = {
      lgShow: false,
      lgEditShow : false,
      lgdelShow : false,
      data : permisosC,
      dataRow : {}
    };
  }

  componentDidMount(){
    this.connectPermisssion();
  }


  async connectPermisssion(){
    await fetch(urlPeople + '/api/permissions')
      .then(response => response.json())
      .then(data => this.setState({ data }));
  }

  showModalEdit(_value){
    this.setState({
      lgEditShow : _value,
    });
  }

  showModalDelete(_value, _dataRow){
    this.setState({
      lgdelShow : _value,
      dataRow : _dataRow
    });
  }





  render (){
    let lgClose = () => this.setState({ lgShow: false });

    return(
      <div>

        {/*<ConfigGeneral />menu de configuracion de tema*/}
        <div id="theme-wrapper">

    		<div id="page-wrapper" className="container">
    			<div className="row">
    			<Navbar activo={1}/>
    				<div id="content-wrapper">
    					<div className="row">
    						<div className="col-lg-12">
    							<div className="row">
    									<Header />
    							</div>
                  <div className="row">
                      <div className="pull-left hidden-xs">
  											<div className="xs-graph pull-left">
  												<div className="graph-label">
  													<img  src="img/pople.png" style={{height:'60px'}} alt=""/>
  												</div>
  											</div>
                        <div className="md-graph pull-left mrg-l-lg mrg-r-sm">
  												<div className="graph-labelCustc">
  													<b style={{fontWeight:300}}>Gestion de usuarios</b>
  												</div>
  											</div>
  											<div className="md-graph pull-left mrg-l-lg mrg-r-sm">
  												<div className="graph-label">
  													<b>|</b> Permisos
  												</div>
  											</div>
  										</div>
                      <div class="pull-right top-page-ui btn-group">
  											<div onClick={() => this.setState({ lgShow: true })} className="pull-right" style={{borderColor:'transparent',backgroundColor:'transparent',display:'flex'}}>
                            <button style={{
                                width: '40px',
                                height: '40px',
                                borderRadius: '5px 0px 0px 5px',
                                backgroundColor: '#2ecc71'
                              }}><i class="fa fa-plus-circle fa-xs"></i></button>
                            <button style={{
                                width: '169px',
                                height: '40px',
                                borderRadius:'0px 5px 5px 0',
                                backgroundColor: '#34495e',
                                fontSize:'16px',
                                textAlign: 'center',
                                verticalAlign: 'middle',
                                lineheight:'40px !important',
                                color:'white'
                              }}>Agregar permiso</button>
  											</div>
  										</div>
                  </div>
    							<div className="row">
                    <div className="col-lg-8 col-md-8 col-xs-12">
                        <div className="main-box-body clearfix">
                          <div className="table-responsive">
                            <Drawtable data={this.state.data} showModalEdit={this.showModalEdit.bind(this) }  showModalDelete={this.showModalDelete.bind(this)} />

                          </div>
                        </div>
                    </div>

    							</div>
    						</div>
    					</div>
              <MyLargeModal show={this.state.lgShow} onHide={lgClose} updateList={this.connectPermisssion.bind(this)}/>
              <MyLargeEditModal show={this.state.lgEditShow} showModalEdit={this.showModalEdit.bind(this)} updateList={this.connectPermisssion.bind(this)} />
              <MyLargeDeleteModal show={this.state.lgdelShow}  showModalDelete={this.showModalDelete.bind(this)} dataRow={this.state.dataRow} updateList={this.connectPermisssion.bind(this)}/>

    					<footer id="footer-bar" className="row">
    						<p id="footer-copyright" className="col-xs-12">

    						</p>
    					</footer>
    				</div>
    			</div>
    		</div>
    	</div>
    </div>
    );
  };
}

// export default Permisos;
