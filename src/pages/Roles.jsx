import $ from 'jquery';
import Popper from 'popper.js';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
//component import
import Navbar from '../component/NavBar';
import Header from '../component/Header';
import ConfigGeneral from '../component/ConfigGeneral';
//component import
const usersC=[
  {id:1,rol:"ACCOUNT MANAGER","permissions": [{
		"name": {
			"S": "Profesores"
		},
		"permissionId": {
			"S": "d35cf4db-6e49-44e3-b72e-d4cbc0a660ae"
		},
		"description": {
			"S": "Puede agregar Profesores"
		},
		"typePermission": {
			"N": "2"
		}
	}, {
		"name": {
			"S": "Administrativos"
		},
		"permissionId": {
			"S": "1b04e4d1-a8ae-40e2-8830-361f336f8bf7"
		},
		"description": {
			"S": "Puede agregar Administrativos"
		},
		"typePermission": {
			"N": "1"
		}
	}, {
		"name": {
			"S": "Alumnos"
		},
		"permissionId": {
			"S": "829eb940-a548-49f1-bf24-e07f5704a6fe"
		},
		"description": {
			"S": "Puede agregar Alumnos"
		},
		"typePermission": {
			"N": "1"
		}
	}]},
  {id:2,rol:"MANAGER","permissions": [{
		"name": {
			"S": "Profesores"
		},
		"permissionId": {
			"S": "d35cf4db-6e49-44e3-b72e-d4cbc0a660ae"
		},
		"description": {
			"S": "Puede agregar Profesores"
		},
		"typePermission": {
			"N": "2"
		}
	}, {
		"name": {
			"S": "Administrativos"
		},
		"permissionId": {
			"S": "1b04e4d1-a8ae-40e2-8830-361f336f8bf7"
		},
		"description": {
			"S": "Puede agregar Administrativos"
		},
		"typePermission": {
			"N": "1"
		}
	}, {
		"name": {
			"S": "Alumnos"
		},
		"permissionId": {
			"S": "829eb940-a548-49f1-bf24-e07f5704a6fe"
		},
		"description": {
			"S": "Puede agregar Alumnos"
		},
		"typePermission": {
			"N": "1"
		}
	}]},
  {id:3,rol:"EMPLEADO","permissions": [{
		"name": {
			"S": "Profesores"
		},
		"permissionId": {
			"S": "d35cf4db-6e49-44e3-b72e-d4cbc0a660ae"
		},
		"description": {
			"S": "Puede agregar Profesores"
		},
		"typePermission": {
			"N": "2"
		}
	}, {
		"name": {
			"S": "Administrativos"
		},
		"permissionId": {
			"S": "1b04e4d1-a8ae-40e2-8830-361f336f8bf7"
		},
		"description": {
			"S": "Puede agregar Administrativos"
		},
		"typePermission": {
			"N": "1"
		}
	}, {
		"name": {
			"S": "Alumnos"
		},
		"permissionId": {
			"S": "829eb940-a548-49f1-bf24-e07f5704a6fe"
		},
		"description": {
			"S": "Puede agregar Alumnos"
		},
		"typePermission": {
			"N": "1"
		}
	}]},
  {id:4,rol:"ALUMNO","permissions": [{
		"name": {
			"S": "Profesores"
		},
		"permissionId": {
			"S": "d35cf4db-6e49-44e3-b72e-d4cbc0a660ae"
		},
		"description": {
			"S": "Puede agregar Profesores"
		},
		"typePermission": {
			"N": "2"
		}
	}, {
		"name": {
			"S": "Administrativos"
		},
		"permissionId": {
			"S": "1b04e4d1-a8ae-40e2-8830-361f336f8bf7"
		},
		"description": {
			"S": "Puede agregar Administrativos"
		},
		"typePermission": {
			"N": "1"
		}
	}, {
		"name": {
			"S": "Alumnos"
		},
		"permissionId": {
			"S": "829eb940-a548-49f1-bf24-e07f5704a6fe"
		},
		"description": {
			"S": "Puede agregar Alumnos"
		},
		"typePermission": {
			"N": "1"
		}
	}]}
];

const permisosC={
	"success": true,
	"permissions": [{
		"name": {
			"S": "Profesores"
		},
		"permissionId": {
			"S": "d35cf4db-6e49-44e3-b72e-d4cbc0a660ae"
		},
		"description": {
			"S": "Puede agregar Profesores"
		},
		"typePermission": {
			"N": "2"
		}
	}, {
		"name": {
			"S": "Administrativos"
		},
		"permissionId": {
			"S": "1b04e4d1-a8ae-40e2-8830-361f336f8bf7"
		},
		"description": {
			"S": "Puede agregar Administrativos"
		},
		"typePermission": {
			"N": "1"
		}
	}, {
		"name": {
			"S": "Alumnos"
		},
		"permissionId": {
			"S": "829eb940-a548-49f1-bf24-e07f5704a6fe"
		},
		"description": {
			"S": "Puede agregar Alumnos"
		},
		"typePermission": {
			"N": "1"
		}
	}]
}
// <button className="square" onClick={function() { alert('click'); }}>
//   {this.props.value}
// </button>
class HeaderCols extends React.Component {
  render() {
    return (<th key={this.props.title} className="text-center"><span>{this.props.title}</span></th>)
  }
}

class RowsRols extends React.Component {

  columnasRend=(nombre,activo,index)=>{
      return(
        <td>
        <div key={"rowdivUp"+index} className="form-group text-center">
            <div key={"rowdivin"+index} className="checkbox-nice checkbox-inline">
              <input key={"checkin"+index} type="checkbox" id={index}/>
              <label key={"labin"+index} htmlFor={index}></label>
            </div>
          </div>
      </td>)
  }

  render() {
    const permisosData=this.props.data.permissions
    return (

      <tr>
        <td>
          <span>{this.props.data.rol}</span>
        </td>
        {permisosData.map((data, index) => {
          return this.columnasRend(data.name.S,data.typePermission.N,this.props.rowI+"r"+index)
        })}

      </tr>
    );
  }
}

class Drawtable extends React.Component {
  //metodos render
  renderRows(data,cols,index) {
    return <RowsRols data={data} columnas={cols} rowI={index}/>;
  }
  renderHeads(titlep) {
    return <HeaderCols title={titlep}/>;
  }

  render() {
    const permisosTit=this.props.data.permissions;
    const rolesData=this.props.roles;
    return (
      <table className="table user-list table-hover" style={{backgroundColor:'white',borderRadius:'5px'}}>
        <thead>
          <tr>
            <th>&nbsp;</th>
              {permisosTit.map((data, index) => {
                return this.renderHeads(data.name.S);
              })}

          </tr>
        </thead>
        <tbody>
            {rolesData.map((data, index) => {
              return this.renderRows(data,permisosTit.length,index);
            })}
        </tbody>
      </table>
    );
  }
}

class Roles extends Component {
  constructor (props){
    super(props);
    this.state={
      data:permisosC,
      usersDummy:[
        {id:1,rol:"ACCOUNT MANAGER",permissions:[]},
        {id:1,rol:"MANAGER",permissions:[]},
        {id:1,rol:"EMPLEADO",permissions:[]},
        {id:1,rol:"ALUMNO",permissions:[]}
      ]
    }
  }


  componentDidMount() {
    fetch("http://54.175.45.28:3200/api/permissions")
    .then(res => res.json()).then(
      (result) => {
        if(result.success==true){
          console.log('success');
          console.log(result);
          let rP=result.permissions
          let jasper = Object.assign([], this.state.usersDummy);    //creating copy of object
          jasper.map((data, index) => {
            console.log('past')
            console.log(jasper[index])
            console.log('future')
            jasper[index].permissions = result.permissions;                        //updating value
          })
          console.log(jasper)
          this.setState({ data:result,usersDummy:jasper});

        }else{
          console.log('success false');
          console.log(result);
          // this.setState({ showError: true,errorMessage:result.error,colorMessage:"red" });
        }
      },
      // Note: it's important to handle errors here
      // instead of a catch() block so that we don't swallow
      // exceptions from actual bugs in components.
      (error) => {
        console.log('ERRORCUSTOM '+error)
      }
    )
  }

  render (){

    return(
      <div>
        {/*<ConfigGeneral />menu de configuracion de tema*/}
        <div id="theme-wrapper">

    		<div id="page-wrapper" className="container">
    			<div className="row">
    			<Navbar activo={1}/>
    				<div id="content-wrapper">
    					<div className="row">
    						<div className="col-lg-12">
    							<div className="row">
                    <Header />
    							</div>
                  <div className="row">
                      <div className="pull-left hidden-xs">
  											<div className="xs-graph pull-left">
  												<div className="graph-label">
  													<img  src="img/pople.png" style={{height:'60px'}} alt=""/>
  												</div>
  											</div>
                        <div className="md-graph pull-left mrg-l-lg mrg-r-sm">
  												<div className="graph-labelCustc">
  													<b style={{fontWeight:300}}>Gestion de usuarios</b>
  												</div>
  											</div>
  											<div className="md-graph pull-left mrg-l-lg mrg-r-sm">
  												<div className="graph-label">
  													<b>|</b> Roles
  												</div>
  											</div>
  										</div>
                  </div>
                  <div className="row">
                    <div className="col-lg-12">
                      {/*<div className="main-box no-header clearfix">*/}
                        <div className="main-box-body clearfix">
                          <div className="table-responsive">


                            <Drawtable data={this.state.data} roles={this.state.usersDummy}/>

                          </div>
                        </div>
                      {/*</div>*/}
                    </div>
                    <div className="col-lg-2">
                    </div>
    							</div>
    						</div>
    					</div>

    					<footer id="footer-bar" className="row">
    						<p id="footer-copyright" className="col-xs-12">

    						</p>
    					</footer>
    				</div>
    			</div>
    		</div>
    	</div>
    </div>
    );
  };
}

export default Roles;
