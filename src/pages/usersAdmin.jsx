import $ from 'jquery';
import Popper from 'popper.js';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import './usersAdmin.css';
import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import {Modal,Button} from 'react-bootstrap';
import { Wizard, Steps, Step } from 'react-albus';
//component import
import Navbar from '../component/NavBar';
import Header from '../component/Header';
import ConfigGeneral from '../component/ConfigGeneral';
//component import

//Eliminar
class MyLargeDeleteModal extends React.Component {
  componentDidMount = () => {
    window.jQuery = $;
    window.$ = $;
    let _this=this;

  }
  render() {
    return (
      <Modal
        {...this.props}
        bsSize="large"
        aria-labelledby="contained-modal-title-lg"
      >
        <Modal.Header>
          <Modal.Title><strong>Eliminar</strong></Modal.Title>
        </Modal.Header>
        <Modal.Body style={{borderRadius:'20px'}}>
          <span>¿Esta seguro que desea al eliminar a este usuario?</span>
        </Modal.Body>
        <Modal.Footer>
          <div class="pull-right btn-group-sm">
            <Button onClick={this.props.onHide}>Close</Button>
            <Button onClick={this.props.onHide} bsStyle="danger">Eliminar</Button>
          </div>
        </Modal.Footer>
      </Modal>
    );
  }
}

//edit
class MyLargeEditModal extends React.Component {
  componentDidMount = () => {
    window.jQuery = $;
    window.$ = $;
    let _this=this;

  }
  render() {
    return (
      <Modal
        {...this.props}
        bsSize="large"
        aria-labelledby="contained-modal-title-lg"
      >
        <Modal.Header>

            <div class="pull-left ">
            <Modal.Title id="contained-modal-title-lg">Datos del usuario</Modal.Title>
            </div>
            <div class="pull-right btn-group-sm">
              <button type="button" onClick={this.props.onHide} className="btn btn-info pull-right" style={{marginLeft:'10px',backgroundColor:'#34495e'}}>
                 Guardar
              </button>
              <button type="button" onClick={this.props.onHide} className="btn btn-default pull-right">
                 Cerrar
              </button>
            </div>
        </Modal.Header>
        <Modal.Body>
          <div className="row">
            <div className="col-lg-4">
              <ul id="progress" className="nodes">
                <li ><div class="node green"></div><p>Datos del usuario</p></li>
                <li ><div class="divider white"></div></li>
                <li ><div class="node grey"></div><p>Permisos</p></li>
                <li ><div class="divider white"></div></li>
                <li ><div class="node grey"></div><p>Contraseña</p></li>
                <li ><div class="divider white"></div></li>
            </ul>

            </div>
            <div className="col-lg-7">
              <div id="myWizard" class="wizard">
                <Wizard>
                   <Steps>
                     <Step
                       id="merlin"
                       render={({ next }) => (
                         <div class="step-pane active" id="step1">
              							<br/>
              							<h4>Datos personales</h4>
              							<div class="form-group">
              								<label class="control-label" for="inputSuccess">Nombre</label>
              								<input type="text" class="form-control" id="inputSuccess"/>

              							</div>
              							<div class="form-group">
              								<label class="control-label" for="inputWarning">Apellido paterno</label>
              								<input type="text" class="form-control" id="inputWarning"/>

              							</div>
              							<div class="form-group">
              								<label class="control-label" for="inputError">Apellido materno</label>
              								<input type="text" class="form-control" id="inputError"/>
              							</div>
                            <br/>
                            <div class="form-group">
              								<label class="control-label" for="inputError">Correo electronico</label>
              								<input type="email" class="form-control" id="inputError"/>
              							</div>
                            <div class="actions btn-group-sm">
                              <button onClick={next} class="btn btn-success btn-mini btn-next" data-last="Finish">Siguiente<i class="icon-arrow-right"></i></button>
                            </div>
              						</div>
                       )}
                     />
                     <Step
                       id="gandalf"
                       render={({ next, previous }) => (
                         <div class="step-pane active" id="step1">
                           <br/>
                           <h4>Premisos</h4>
                           <div class="form-group">
                             <div class="form-group col-xs-3">
                               <label class="switch">Account manager</label>
                               <label class="switch">
                                 <input type="checkbox"/>
                                 <span class="slider round"></span>
                               </label>
                             </div>
                              <div class="form-group col-xs-3">
                                <label class="switch">Account manager</label>
                                <label class="switch">
                                  <input type="checkbox"/>
                                  <span class="slider round"></span>
                                </label>
     													</div>
                              <div class="form-group col-xs-3">
                                <label class="switch">Account manager</label>
                                <label class="switch">
                                  <input type="checkbox" checked/>
                                  <span class="slider round"></span>
                                </label>
     													</div>
     													<div class="form-group col-xs-3">
                                <label class="switch">Account manager</label>
                                <label class="switch">
                                  <input type="checkbox" checked/>
                                  <span class="slider round"></span>
                                </label>
     													</div>
               							</div>
                            <div class="form-group">
                              <div class="form-group col-xs-3">
                                <label class="switch">Account manager</label>
                                <label class="switch">
                                  <input type="checkbox"/>
                                  <span class="slider round"></span>
                                </label>
                              </div>
                               <div class="form-group col-xs-3">
                                 <label class="switch">manager</label>
                                 <label class="switch">
                                   <input type="checkbox"/>
                                   <span class="slider round"></span>
                                 </label>
      													</div>
                               <div class="form-group col-xs-3">
                                 <label class="switch">edit courses</label>
                                 <label class="switch">
                                   <input type="checkbox" checked/>
                                   <span class="slider round"></span>
                                 </label>
      													</div>
      													<div class="form-group col-xs-3">
                                 <label class="switch">Account manager</label>
                                 <label class="switch">
                                   <input type="checkbox"/>
                                   <span class="slider round"></span>
                                 </label>
      													</div>
                							</div>

                           <div class="actions btn-group-sm col-xs-12">
                              <button onClick={previous} class="btn btn-default btn-mini btn-prev"> <i class="icon-arrow-left"></i>Anterior</button>
                              <button onClick={next} class="btn btn-success btn-mini btn-next" data-last="Finish">Siguiente<i class="icon-arrow-right"></i></button>
                          </div>
              					 </div>
                       )}
                     />
                     <Step
                       id="dumbledore2"
                       render={({ next, previous}) => (
                         <div class="step-pane" id="step3">
            							<br/>
            							<h4>Cambiar Contraseña</h4>
                          <span class="help-block"> (Deje en blanco la contraseña si no desea cambiarla)</span>
            							<div class="form-group">
            								<label for="maskedDate">nueva contraseña</label>
            								<div class="input-group">
            									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            									<input type="text" class="form-control" id="maskedDate"/>
            								</div>
            							</div>

            							<div class="form-group">
            								<label for="maskedPhone">Confirmar contraseña</label>
            								<div class="input-group">
            									<span class="input-group-addon"><i class="fa fa-phone"></i></span>
            									<input type="text" class="form-control" id="maskedPhone"/>
            								</div>
            							</div>

                          <div class="actions btn-group-sm">
                             <button onClick={previous} class="btn btn-default btn-mini btn-prev"> <i class="icon-arrow-left"></i>Anterior</button>
                             <button onClick={next} class="btn btn-success btn-mini btn-next" data-last="Finish">Siguiente<i class="icon-arrow-right"></i></button>
                         </div>
            						</div>
                       )}
                     />
                     <Step
                       id="dumbledore4"
                       render={({ previous }) => (
                         <div class="step-pane" id="step3">
                           <br/>
               							<h4>Finalizar Registro</h4>

               							<div class="alert alert-info fade in" style={{margin: '100px 0'}}>
               								<i class="fa fa-check-circle fa-fw fa-lg"></i>
               								<strong>Ha terminado la captura</strong> pulse el boton guardar para terminar el registro.
               							</div>
                            <div class="actions btn-group-sm">
                               <button onClick={previous} class="btn btn-default btn-mini btn-prev"> <i class="icon-arrow-left"></i>Anterior</button>
                           </div>
                        </div>
                       )}
                     />
                   </Steps>
                 </Wizard>
                </div>
             </div>
           </div>
        </Modal.Body>
        <Modal.Footer>

        </Modal.Footer>
      </Modal>
    );
  }
}
//registro
class MyLargeModal extends React.Component {
  componentDidMount = () => {
    window.jQuery = $;
    window.$ = $;
    let _this=this;

  }
  render() {
    return (
      <Modal
        {...this.props}
        bsSize="large"
        aria-labelledby="contained-modal-title-lg"
      >
        <Modal.Header>

            <div class="pull-left ">
            <Modal.Title id="contained-modal-title-lg">Datos del usuario</Modal.Title>
            </div>
            <div class="pull-right btn-group-sm">
              <button type="button" onClick={this.props.onHide} className="btn btn-info pull-right" style={{marginLeft:'10px',backgroundColor:'#34495e'}}>
                 Guardar
              </button>
              <button type="button" onClick={this.props.onHide} className="btn btn-default pull-right">
                 Cerrar
              </button>
            </div>
        </Modal.Header>
        <Modal.Body>
          <div className="row">
            <div className="col-lg-4">
              <ul id="progress" className="nodes">
                <li ><div class="node green"></div><p>Datos del usuario</p></li>
                <li ><div class="divider white"></div></li>
                <li ><div class="node grey"></div><p>Permisos</p></li>
                <li ><div class="divider white"></div></li>
                <li ><div class="node grey"></div><p>Contraseña</p></li>
                <li ><div class="divider white"></div></li>
            </ul>

            </div>
            <div className="col-lg-7">
              <div id="myWizard" class="wizard">
                <Wizard>
                   <Steps>
                     <Step
                       id="merlin"
                       render={({ next }) => (
                         <div class="step-pane active" id="step1">
              							<br/>
              							<h4>Datos personales</h4>
              							<div class="form-group">
              								<label class="control-label" for="inputSuccess">Nombre</label>
              								<input type="text" class="form-control" id="inputSuccess"/>

              							</div>
              							<div class="form-group">
              								<label class="control-label" for="inputWarning">Apellido paterno</label>
              								<input type="text" class="form-control" id="inputWarning"/>

              							</div>
              							<div class="form-group">
              								<label class="control-label" for="inputError">Apellido materno</label>
              								<input type="text" class="form-control" id="inputError"/>
              							</div>
                            <br/>
                            <div class="form-group">
              								<label class="control-label" for="inputError">Correo electronico</label>
              								<input type="email" class="form-control" id="inputError"/>
              							</div>
                            <div class="actions btn-group-sm">
                              <button onClick={next} class="btn btn-success btn-mini btn-next" data-last="Finish">Siguiente<i class="icon-arrow-right"></i></button>
                            </div>
              						</div>
                       )}
                     />
                     <Step
                       id="gandalf"
                       render={({ next, previous }) => (
                         <div class="step-pane active" id="step1">
                           <br/>
                           <h4>Premisos</h4>
                           <div class="form-group">
                             <div class="form-group col-xs-3">
                               <label class="switch">Account manager</label>
                               <label class="switch">
                                 <input type="checkbox"/>
                                 <span class="slider round"></span>
                               </label>
                             </div>
                              <div class="form-group col-xs-3">
                                <label class="switch">Account manager</label>
                                <label class="switch">
                                  <input type="checkbox"/>
                                  <span class="slider round"></span>
                                </label>
     													</div>
                              <div class="form-group col-xs-3">
                                <label class="switch">Account manager</label>
                                <label class="switch">
                                  <input type="checkbox" checked/>
                                  <span class="slider round"></span>
                                </label>
     													</div>
     													<div class="form-group col-xs-3">
                                <label class="switch">Account manager</label>
                                <label class="switch">
                                  <input type="checkbox" checked/>
                                  <span class="slider round"></span>
                                </label>
     													</div>
               							</div>
                            <div class="form-group">
                              <div class="form-group col-xs-3">
                                <label class="switch">Account manager</label>
                                <label class="switch">
                                  <input type="checkbox"/>
                                  <span class="slider round"></span>
                                </label>
                              </div>
                               <div class="form-group col-xs-3">
                                 <label class="switch">manager</label>
                                 <label class="switch">
                                   <input type="checkbox"/>
                                   <span class="slider round"></span>
                                 </label>
      													</div>
                               <div class="form-group col-xs-3">
                                 <label class="switch">edit courses</label>
                                 <label class="switch">
                                   <input type="checkbox" checked/>
                                   <span class="slider round"></span>
                                 </label>
      													</div>
      													<div class="form-group col-xs-3">
                                 <label class="switch">Account manager</label>
                                 <label class="switch">
                                   <input type="checkbox"/>
                                   <span class="slider round"></span>
                                 </label>
      													</div>
                							</div>

                           <div class="actions btn-group-sm col-xs-12">
                              <button onClick={previous} class="btn btn-default btn-mini btn-prev"> <i class="icon-arrow-left"></i>Anterior</button>
                              <button onClick={next} class="btn btn-success btn-mini btn-next" data-last="Finish">Siguiente<i class="icon-arrow-right"></i></button>
                          </div>
              					 </div>
                       )}
                     />
                     <Step
                       id="dumbledore2"
                       render={({ next, previous}) => (
                         <div class="step-pane" id="step3">
            							<br/>
            							<h4>Cambiar Contraseña</h4>

            							<div class="form-group">
            								<label for="maskedDate">nueva contraseña</label>
            								<div class="input-group">
            									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            									<input type="text" class="form-control" id="maskedDate"/>
            								</div>
            							</div>

            							<div class="form-group">
            								<label for="maskedPhone">Confirmar contraseña</label>
            								<div class="input-group">
            									<span class="input-group-addon"><i class="fa fa-phone"></i></span>
            									<input type="text" class="form-control" id="maskedPhone"/>
            								</div>
            							</div>

                          <div class="actions btn-group-sm">
                             <button onClick={previous} class="btn btn-default btn-mini btn-prev"> <i class="icon-arrow-left"></i>Anterior</button>
                             <button onClick={next} class="btn btn-success btn-mini btn-next" data-last="Finish">Siguiente<i class="icon-arrow-right"></i></button>
                         </div>
            						</div>
                       )}
                     />
                     <Step
                       id="dumbledore4"
                       render={({ previous }) => (
                         <div class="step-pane" id="step3">
                           <br/>
               							<h4>Finalizar Registro</h4>

               							<div class="alert alert-info fade in" style={{margin: '100px 0'}}>
               								<i class="fa fa-check-circle fa-fw fa-lg"></i>
               								<strong>Ha terminado la captura</strong> pulse el boton guardar para terminar el registro.
               							</div>
                            <div class="actions btn-group-sm">
                               <button onClick={previous} class="btn btn-default btn-mini btn-prev"> <i class="icon-arrow-left"></i>Anterior</button>
                           </div>
                        </div>
                       )}
                     />
                   </Steps>
                 </Wizard>
                </div>
             </div>
           </div>
        </Modal.Body>
        <Modal.Footer>

        </Modal.Footer>
      </Modal>
    );
  }
}
class UserAdmin extends Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      lgShow: false,
      lgEditShow : false,
      lgdelShow : false
    };
  }
  render (){
    let lgClose = () => this.setState({ lgShow: false });
    let lgEditClose = () => this.setState({ lgEditShow: false });
    let lgDelClose = () => this.setState({ lgdelShow: false });

    return(
      <div>

        {/*<ConfigGeneral />menu de configuracion de tema*/}
        <div id="theme-wrapper">

    		<div id="page-wrapper" className="container">
    			<div className="row">
    			<Navbar activo={1}/>
    				<div id="content-wrapper">
    					<div className="row">
    						<div className="col-lg-12">
    							<div className="row">
    									<Header />
    							</div>
                  <div className="row">
                      <div className="pull-left hidden-xs">
  											<div className="xs-graph pull-left">
  												<div className="graph-label">
  													<img  src="img/pople.png" style={{height:'60px'}} alt=""/>
  												</div>
  											</div>
                        <div className="md-graph pull-left mrg-l-lg mrg-r-sm">
  												<div className="graph-labelCustc">
  													<b style={{fontWeight:300}}>Gestion de usuarios</b>
  												</div>
  											</div>
  											<div className="md-graph pull-left mrg-l-lg mrg-r-sm">
  												<div className="graph-label">
  													<b>|</b> Administrativo
  												</div>
  											</div>
  										</div>
                      <div class="pull-right top-page-ui btn-group">
  											<div onClick={() => this.setState({ lgShow: true })} className="pull-right" style={{borderColor:'transparent',backgroundColor:'transparent',display:'flex'}}>
                            <button style={{
                                width: '40px',
                                height: '40px',
                                borderRadius: '5px 0px 0px 5px',
                                backgroundColor: '#2ecc71'
                              }}><i class="fa fa-plus-circle fa-xs"></i></button>
                            <button style={{
                                width: '169px',
                                height: '40px',
                                borderRadius:'0px 5px 5px 0',
                                backgroundColor: '#34495e',
                                fontSize:'16px',
                                textAlign: 'center',
                                verticalAlign: 'middle',
                                lineheight:'40px !important',
                                color:'white'
                              }}>Añadir admin</button>
  											</div>
  										</div>
                  </div>
    							<div className="row">
                    <div className="col-lg-12 col-md-12 col-xs-12">
                        <div className="main-box-body clearfix">
                          <div className="table-responsive">
                            <table className="table user-list table-hover">
                              <thead>
                                <tr>
                                  <th><span>Nombre</span></th>
                                  <th><span>Apellido paterno</span></th>
                                  <th><span>Apellido materno</span></th>
                                  <th className="text-center" style={{borderLeft:' 1px solid #000'}}><span>Puesto</span></th>
                                  <th>editar</th>
                                  <th>&nbsp;</th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>
                                    <span>ISAAC NICOLAS</span>
                                  </td>
                                  <td>
                                    RAMIREZ
                                  </td>
                                  <td className="text-center">
                                    GONZALEZ
                                  </td>
                                  <td style={{borderLeft:' 1px solid #000'}}>
                                    <span className="user-subhead">Admin</span>
                                  </td>
                                  <td>
                                    <span onClick={() => this.setState({ lgEditShow: true })}  class="fa-stack" style={{borderRadius:'4px',backgroundColor:'blue'}}>
  																		<i class="fa fa-pencil fa-stack-1x"></i>
  																	</span>
                                  </td>
                                  <td>
                                    <span onClick={() => this.setState({ lgdelShow: true })} class="fa-stack" style={{borderRadius:'4px',backgroundColor:'red'}}>
  																		<i class="fa fa-trash-o fa-stack-1x"></i>
  																	</span>
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <span>JUAN MANUEL</span>
                                  </td>
                                  <td>
                                    CORTES
                                  </td>
                                  <td className="text-center">
                                    PAREDES
                                  </td>
                                  <td style={{borderLeft:' 1px solid #000'}}>
                                    <span className="user-subhead">Admin</span>
                                  </td>
                                  <td>
                                    <span onClick={() => this.setState({ lgEditShow: true })}  class="fa-stack" style={{borderRadius:'4px',backgroundColor:'blue'}}>
  																		<i class="fa fa-pencil fa-stack-1x"></i>
  																	</span>
                                  </td>
                                  <td>
                                    <span onClick={() => this.setState({ lgdelShow: true })} class="fa-stack" style={{borderRadius:'4px',backgroundColor:'red'}}>
  																		<i class="fa fa-trash-o fa-stack-1x"></i>
  																	</span>
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <span>ANDRES HIPOLITO</span>
                                  </td>
                                  <td>
                                    GUTIERREZ
                                  </td>
                                  <td className="text-center">
                                    GUTIERREZ
                                  </td>
                                  <td style={{borderLeft:' 1px solid #000'}}>
                                    <span className="user-subhead">Admin</span>
                                  </td>
                                  <td>
                                    <span onClick={() => this.setState({ lgEditShow: true })}  class="fa-stack" style={{borderRadius:'4px',backgroundColor:'blue'}}>
  																		<i class="fa fa-pencil fa-stack-1x"></i>
  																	</span>
                                  </td>
                                  <td>
                                    <span onClick={() => this.setState({ lgdelShow: true })} class="fa-stack" style={{borderRadius:'4px',backgroundColor:'red'}}>
  																		<i class="fa fa-trash-o fa-stack-1x"></i>
  																	</span>
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <span>MAGALY</span>
                                  </td>
                                  <td>
                                    ALBA
                                  </td>
                                  <td className="text-center">
                                    MARTINEZ
                                  </td>
                                  <td style={{borderLeft:' 1px solid #000'}}>
                                    <span className="user-subhead">Admin</span>
                                  </td>
                                  <td>
                                    <span onClick={() => this.setState({ lgEditShow: true })}  class="fa-stack" style={{borderRadius:'4px',backgroundColor:'blue'}}>
  																		<i class="fa fa-pencil fa-stack-1x"></i>
  																	</span>
                                  </td>
                                  <td>
                                    <span onClick={() => this.setState({ lgdelShow: true })} class="fa-stack" style={{borderRadius:'4px',backgroundColor:'red'}}>
  																		<i class="fa fa-trash-o fa-stack-1x"></i>
  																	</span>
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <span>MADIAN</span>
                                  </td>
                                  <td>
                                    FLORES
                                  </td>
                                  <td className="text-center">
                                    CÁRDENAS
                                  </td>
                                  <td style={{borderLeft:' 1px solid #000'}}>
                                    <span className="user-subhead">Admin</span>
                                  </td>
                                  <td>
                                    <span onClick={() => this.setState({ lgEditShow: true })}  class="fa-stack" style={{borderRadius:'4px',backgroundColor:'blue'}}>
  																		<i class="fa fa-pencil fa-stack-1x"></i>
  																	</span>
                                  </td>
                                  <td>
                                    <span onClick={() => this.setState({ lgdelShow: true })} class="fa-stack" style={{borderRadius:'4px',backgroundColor:'red'}}>
  																		<i class="fa fa-trash-o fa-stack-1x"></i>
  																	</span>
                                  </td>

                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                    </div>

    							</div>
    						</div>
    					</div>
              <MyLargeModal show={this.state.lgShow} onHide={lgClose} />
              <MyLargeEditModal show={this.state.lgEditShow} onHide={lgEditClose} />
              <MyLargeDeleteModal show={this.state.lgdelShow} onHide={lgDelClose} />

    					<footer id="footer-bar" className="row">
    						<p id="footer-copyright" className="col-xs-12">

    						</p>
    					</footer>
    				</div>
    			</div>
    		</div>
    	</div>
    </div>
    );
  };
}

export default UserAdmin;
