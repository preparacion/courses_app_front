import React, { Component } from 'react';
import { 
  BrowserRouter as Router,
  Route,
  Link,
  Redirect,
  withRouter 
} from "react-router-dom";
import {Tabs, Tab, NavItem, Row, Nav, Col, Alert} from 'react-bootstrap';
import { urlCourses, urlPeople}  from './Globals';
// var Globals = require('./Globals');
// alert(Globals.urlCourses);
//component import
// import Navbar from '../component/NavBar';

//component import
// validar si se mostrara el div de error
const withErrorHandling = WrappedComponent => ({ showError,children,messageColor }) => {
  var colors=(messageColor=="red") ? "alert alert-danger":"alert alert-success"
  return (
      <WrappedComponent>
        {/* si show error es true se agrega el div con el mensage de error enviado en los props en DivWithErrorHandling en el form */}
        {showError && <div className={colors}>{children}</div>}
      </WrappedComponent>
    );
};

// declarar el controlador del div de error
const DivWithErrorHandling = withErrorHandling(({children}) => <div>{children}</div>)

class Login extends Component {

  constructor (props){
    super(props);
    this.state={
      mail:"",
      auth:false,
      mail:"",
      newmail:"",
      setPass:"",
      setPassc:"",
      pass:"",
      colorMessage:"",
      errorMessage: "",
      showError: "",
    }
  }

  onKeyUp(event) {
    //actualizar state al teclear en los campos de login
    const value = event.target.value;
    this.setState({[event.target.name]:event.target.value})
    // console.log(event.target);
  }

  flogin = (e) => {

    e.preventDefault();
    var headerC = new Headers({
         'x-app': 'backend',
         'x-access-token': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoidXNlciIsImFwcCI6ImJhY2tlbmQiLCJzaXRlIjoiaHR0cHM6Ly93d3cudGl6emthLmNvbSIsInZlcnNpb24iOiIxLjIuMyIsImlhdCI6MTUwNjk2NTIyN30.wJdFGbhl8FLUEclvlnN6oX2Dj5Gvy91WAtRxHCMY8ek',
         'Content-Type': 'application/json',
         'user-id': 'PYmu0S8kal',
         'Access-Control-Allow-Origin':'*'
   });
   // var data = {"email": "foo@bar.com","password": "1234"};
   fetch(urlCourses + "/api/login", {
     method: 'POST',
     body: '{"email": "'+this.state.mail+'","password": "'+this.state.pass+'"}',
     headers: headerC
   })
     .then(res => res.json()).then(
       (result) => {
         if(result.success == true){
           sessionStorage.setItem('auth',true);
           this.setState({ auth: true });
         }else{
           console.log(result)
           this.setState({ showError: true, errorMessage: result.error, colorMessage: "red" });
           ///continuar con el login solo test
          //  sessionStorage.setItem('auth',true);
          //  this.setState({ auth: true });
         }
         // this.setState({ showError: !this.state.showError,errorMessage:result.error });

       },
       // Note: it's important to handle errors here
       // instead of a catch() block so that we don't swallow
       // exceptions from actual bugs in components.
       (error) => {
         console.log('ERRORCUSTOM '+error)
         // this.setState({ showError: true,errorMessage: error,colorMessage:"red"});

         ///continuar con el login solo test
         sessionStorage.setItem('auth',true);
         this.setState({ auth: true });
       }
     )

  }

  fregister=(e)=>{

    e.preventDefault();
    let mailrev=this.state.newmail;
    var emailValid = mailrev.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
    alert(emailValid);
    if(!emailValid){
      this.setState({ showError: !this.state.showError,errorMessage:"El formato de correo no es valido.",colorMessage:"red"});
      return;
    }

    if(this.state.setPass != ""){
      if(this.state.setPass != this.state.setPassc){
        this.setState({ showError: true,errorMessage:"Las contraseñas no coinciden.",colorMessage:"red"});
        return;
      }
    }else{
      this.setState({ showError: true,errorMessage:"Escriba una contraseña.",colorMessage:"red"});
      return;
    }
    // console.log('{"name": "'+this.state.setname+'","lastName": "'+this.state.setlastname+'","email": "'+this.state.newmail+'","password": "'+this.state.setPass+'"}');

    var headerC = new Headers({
         'x-app': 'backend',
         'x-access-token': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoidXNlciIsImFwcCI6ImJhY2tlbmQiLCJzaXRlIjoiaHR0cHM6Ly93d3cudGl6emthLmNvbSIsInZlcnNpb24iOiIxLjIuMyIsImlhdCI6MTUwNjk2NTIyN30.wJdFGbhl8FLUEclvlnN6oX2Dj5Gvy91WAtRxHCMY8ek',
         'Content-Type': 'application/json',
         'user-id': 'PYmu0S8kal',
         'Access-Control-Allow-Origin':'*'
   });
   // var data = {"email": "foo@bar.com","password": "1234"};
   fetch("http://54.175.45.28/api/register", {
     method: 'POST',
     body: '{"name": "'+this.state.setname+'","lastName": "'+this.state.setlastname+'","email": "'+this.state.newmail+'","password": "'+this.state.setPass+'"}',
     headers: headerC
   })
     .then(res => res.json()).then(
       (result) => {
         if(result.success==true){
           // sessionStorage.setItem('auth',true);
           // this.setState({ auth: true });
           this.setState({ showError: true,errorMessage:"Registro correcto.",colorMessage:"green"});
         }else{
           console.log(result)
           this.setState({ showError: true,errorMessage:result.error,colorMessage:"red"});
         }

       },
       // Note: it's important to handle errors here
       // instead of a catch() block so that we don't swallow
       // exceptions from actual bugs in components.
       (error) => {
         console.log('ERRORCUSTOM '+error)
         this.setState({ showError: true,errorMessage: error,colorMessage:"red"});
       }
     )

  }


  render (){
    console.log(this.props)
    let redirectToReferrer = this.state.auth;
    if (redirectToReferrer) {
      // console.log('redirect');
      // console.log(redirectToReferrer);
      return <Redirect to="/Home" />;
    }
    return(
      <div className="container loginImage" >

    		<div className="row">


    			<div className="col-xs-12">
    				<div id="login-box">
    					<div id="login-box-holder">
    						<div className="row">
    							<div className="col-xs-12">
    								<header id="login-header" >
    									<div id="login-logo" style={{borderRadius:'15px 15px 0px 0px'}}>
    										<img src="img/logo.png" alt=""/>
    									</div>
                      <div className="row" style={{backgroundColor:'#fbfbfb',marginTop:'-40px'}}>
                        <div className="col-xs-1 col-sm-1" >
                        </div>
                        <div className="col-xs-10 col-sm-10" >
                          <button type="submit" className="btn btn-primary col-xs-12 btn-facebook" style={{borderRadius:'10px',height:'50px',fontFamily:'helvetica',fontSize:'18px'}}>
                            Conecta con Facebook
                          </button>
                        </div>
                        <div className="col-xs-1 col-sm-1" >
                        </div>
                      </div>

    								</header>

    								<div id="login-box-inner" style={{borderRadius:'0px 0px 15px 15px'}}>
                      <img src="img/separator-login.png" alt="" style={{marginLeft:'auto',marginRight: 'auto',display: 'block',maxWidth: '350px'}}/>
                      <DivWithErrorHandling showError={this.state.showError} children={this.state.errorMessage} messageColor={this.state.colorMessage}/>
                      <Tab.Container id="left-tabs-example" defaultActiveKey="first">
                        <Row className="clearfix">
                          <Col sm={12}>
                            <Nav justified bsStyle="tabs" activeKey="first" bsClass="custom-tab">
                              <NavItem eventKey="first" >Regístrate</NavItem>
                              <NavItem eventKey="second">Tengo una cuenta</NavItem>
                            </Nav>
                          </Col>
                          <Col sm={12}>
                            <Tab.Content animation>
                              <Tab.Pane eventKey="first">
                                <form role="form" action="registration.html">
                                  <div className="form-group">
                                    <label htmlFor="exampleInputEmail1">Nombre</label>
                										<input name="setname" onKeyUp={event=>this.onKeyUp(event)} className="form-control custom-input-form" type="text" placeholder="Nombre" />
                									</div>
                                  <div className="form-group">
                                    <label htmlFor="exampleInputEmail1">Apellido</label>
                										<input name="setlastname" onKeyUp={event=>this.onKeyUp(event)} className="form-control custom-input-form" type="text" placeholder="Apellido" />
                									</div>
                									<div className="form-group">
                                    <label htmlFor="exampleInputEmail1">Tu correo electrónico</label>
                										<input name="newmail" onKeyUp={event=>this.onKeyUp(event)} className="form-control custom-input-form" type="text" placeholder="Tu Correo" />
                									</div>
                									<div className="form-group">
                                    <label htmlFor="exampleInputpass1">Elige contraseña</label>
                										<input name="setPass" onKeyUp={event=>this.onKeyUp(event)} type="password" className="form-control custom-input-form" placeholder="Contraseña" />
                									</div>
                									<div className="form-group">
                                    <label htmlFor="exampleInputpass2">Confirma tu contraseña</label>
                										<input name="setPassc" onKeyUp={event=>this.onKeyUp(event)} type="password" className="form-control custom-input-form" placeholder="Repite tu contraseña" />
                									</div>
                									{/*<div id="remember-me-wrapper">
                										<div className="row">
                											<div className="col-xs-12">
                												<div className="checkbox-nice">
                													<input type="checkbox" id="terms-cond" checked="checked" />
                													<label htmlFor="terms-cond">
                														Acepto los terminos y condiciones
                													</label>
                												</div>
                											</div>
                										</div>
                									</div>*/}
                									<div className="row">
                										<div className="col-xs-12">
                											<button onClick={e=>this.fregister(e)} className="btn btn-success col-xs-5 pull-right" style={{fontSize:'14px',fontWeight:'regular',fontFamily:'helvetica',backgroundColor:'#4caf50'}}>Crear cuenta</button>
                										</div>
                									</div>
                								</form>
                              </Tab.Pane>
                              <Tab.Pane eventKey="second">
                                {/*DivWithErrorHandling mostara el mensaje de error al actualizarse el state*/}
                                <form role="form" action="index.html">
              										<div className="form-group">
                                    <label htmlFor="exampleInputEmail2">Tu correo electrónico</label>
              											<input name="mail" onKeyUp={event=>this.onKeyUp(event)} className="form-control custom-input-form" type="text" placeholder="Correo"/>
              										</div>
              										<div className="form-group">
                                    <label htmlFor="exampleInputPassLog">Tu contraseña</label>
              											<input name="pass" onKeyUp={event=>this.onKeyUp(event)} type="password" className="form-control custom-input-form" placeholder="Contraseña"/>
              										</div>
              										<div id="remember-me-wrapper">
              											<div className="row">
              												<div className="col-xs-6">
              													<div className="checkbox-nice">
              														<input type="checkbox" id="remember-me" defaultChecked />
              														<label htmlFor="remember-me">
              															Remember me
              														</label>
              													</div>
              												</div>
              												<a href="forgot-password.html" id="login-forget-link" className="col-xs-6">
              													Forgot password?
              												</a>
              											</div>
              										</div>
              										<div className="row">
              											<div className="col-xs-12">
              												{/*<Link to="/Home" className="btn btn-success col-xs-5 pull-right" style={{fontSize:'14px',fontWeight:'regular',fontFamily:'helvetica',backgroundColor:'#4caf50'}}>Entrar</Link>*/}
                                      <button onClick={(e)=>this.flogin(e)} className="btn btn-success col-xs-5 pull-right" style={{fontSize:'14px',fontWeight:'regular',fontFamily:'helvetica',backgroundColor:'#4caf50'}}>Entrar</button>
              											</div>
              										</div>
              									</form>
                              </Tab.Pane>
                            </Tab.Content>
                          </Col>
                        </Row>
                      </Tab.Container>
    								</div>
    							</div>
    						</div>
    					</div>

    					<div id="login-box-footer">
    						<div className="row">
    							<div className="col-xs-12">
    								footer
    							</div>
    						</div>
    					</div>
    				</div>
    			</div>
    		</div>
    	</div>
    );
  };
}

export default Login;
