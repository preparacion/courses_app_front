import $ from 'jquery';
import Popper from 'popper.js';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
//component import
import Navbar from '../component/NavBar';
import Header from '../component/Header';
import ConfigGeneral from '../component/ConfigGeneral';
//component import

class Asistencia extends Component {
  render (){
    return(
      <div>
        {/*<Header />cabecera*/}
        {/*<ConfigGeneral />menu de configuracion de tema*/}
        <div id="theme-wrapper">

    		<div id="page-wrapper" className="container">
    			<div className="row">
    			<Navbar activo={1}/>
    				<div id="content-wrapper">
    					<div className="row">
    						<div className="col-lg-12">
                  <div className="row">
    									<Header />
    							</div>
                  <div className="row">
                      <div class="pull-left hidden-xs">
  											<div class="xs-graph pull-left">
  												<div class="graph-label">
  													<img  src="img/pople.png" style={{height:'60px'}} alt=""/>
  												</div>
  											</div>
                        <div class="md-graph pull-left mrg-l-lg mrg-r-sm">
  												<div class="graph-label">
  													<b style={{fontWeight:300}}>Gestion de usuarios</b>
  												</div>
  											</div>
  											<div class="md-graph pull-left mrg-l-lg mrg-r-sm">
  												<div class="graph-label">
  													<b>|</b> Asistencia
  												</div>
  											</div>
  										</div>
                  </div>
    							<div className="row">

                    <div className="col-lg-7">
                      {/*<div className="main-box no-header clearfix">*/}
                        <div className="main-box-body clearfix">
                          <div className="table-responsive">
                            <table id="table-example-fixed" className="table table-hover">
                              <thead>
                                <tr>
                                  <th>Alumno</th>
                                  <th>Telefono</th>
                                  <th>Correo</th>
                                  <th>Asistencias</th>
                                  <th>faltas</th>

                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>JOSUE LÓPEZ SANTOS</td>
                                  <td>555667758</td>
                                  <td>mail@test.com</td>
                                  <td>61</td>
                                  <td>0</td>
                                </tr>
                                <tr>
                                  <td>Alejandro Rodriguez</td>
                                  <td>555667758</td>
                                  <td>mail@test.com</td>
                                  <td>63</td>
                                  <td>0</td>

                                </tr>
                                <tr>
                                  <td>Erandi Elizabeth Salgado</td>
                                  <td>555667758</td>
                                  <td>mail@test.com</td>
                                  <td>66</td>
                                  <td>0</td>

                                </tr>
                                <tr>
                                  <td>Uriel Rodríguez</td>
                                  <td>555667758</td>
                                  <td>mail@test.com</td>
                                  <td>22</td>
                                  <td>0</td>

                                </tr>
                                <tr>
                                  <td>Sandra Cayetano</td>
                                    <td>555667758</td>
                                    <td>mail@test.com</td>
                                  <td>33</td>
                                  <td>0</td>

                                </tr>
                                <tr>
                                  <td>Diego Ivan Hernández</td>
                                    <td>555667758</td>
                                    <td>mail@test.com</td>
                                  <td>61</td>
                                  <td>0</td>

                                </tr>
                                <tr>
                                  <td>Monica Martínez</td>
                                    <td>555667758</td>
                                    <td>mail@test.com</td>
                                  <td>59</td>
                                  <td>0</td>

                                </tr>
                                <tr>
                                  <td>Hugo Sanchez</td>
                                    <td>555667758</td>
                                    <td>mail@test.com</td>
                                  <td>55</td>
                                  <td>0</td>

                                </tr>
                                <tr>
                                  <td>salvador Muñiz</td>
                                    <td>555667758</td>
                                    <td>mail@test.com</td>
                                  <td>39</td>
                                  <td>0</td>

                                </tr>
                                <tr>
                                  <td>Sergio Garrido</td>
                                    <td>555667758</td>
                                    <td>mail@test.com</td>
                                  <td>23</td>
                                  <td>0</td>

                                </tr>
                                <tr>
                                  <td>Emmelin López</td>
                                    <td>555667758</td>
                                    <td>mail@test.com</td>
                                  <td>30</td>
                                  <td>0</td>

                                </tr>
                                <tr>
                                  <td>Ariana Bello</td>
                                  <td>Support Lead</td>
                                  <td>Edinburgh</td>
                                  <td>22</td>
                                  <td>0</td>

                                </tr>
                                <tr>
                                  <td>Hector Miguel Juarez</td>
                                    <td>555667758</td>
                                    <td>mail@test.com</td>
                                  <td>36</td>
                                  <td>0</td>

                                </tr>
                                <tr>
                                  <td>Luis Gonzalez</td>
                                    <td>555667758</td>
                                    <td>mail@test.com</td>
                                  <td>43</td>
                                  <td>0</td>

                                </tr>
                                <tr>
                                  <td>oscar sandoval</td>
                                    <td>555667758</td>
                                    <td>mail@test.com</td>
                                  <td>19</td>
                                  <td>0</td>

                                </tr>
                                <tr>
                                  <td>erika ariadna rojas</td>
                                    <td>555667758</td>
                                    <td>mail@test.com</td>
                                  <td>66</td>
                                  <td>0</td>

                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      {/*</div>*/}
                    </div>

    							</div>
    						</div>
    					</div>

    					<footer id="footer-bar" className="row">
    						<p id="footer-copyright" className="col-xs-12">

    						</p>
    					</footer>
    				</div>
    			</div>
    		</div>
    	</div>
    </div>
    );
  };
}

export default Asistencia;
