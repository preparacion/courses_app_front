import $ from 'jquery';
import Popper from 'popper.js';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
//component import
import Navbar from '../component/NavBar';
//component import

class AdmEsc extends Component {
  render (){
    return(
      <div className="page-wrapper">
        <Navbar activo={4}/>
          {/* PAGE CONTAINER */}
        <div className="page-container2">
            {/* HEADER DESKTOP*/}
            <header className="header-desktop2">
                <div className="section__content section__content--p30">
                    <div className="container-fluid">
                        <div className="header-wrap2">
                            <div className="logo d-block d-lg-none">
                                <Link to={{
                                pathname: '/',
                                state: { fromDashboard: true }
                                }}>
                                    <img src="images/icon/logo-white.png" alt="CoolAdmin" />
                                </Link>
                            </div>
                            <div className="header-button2">
                                <div className="header-button-item js-item-menu">
                                    <i className="zmdi zmdi-search"></i>
                                    <div className="search-dropdown js-dropdown">
                                        <form action="">
                                            <input className="au-input au-input--full au-input--h65" type="text" placeholder="Search for datas &amp; reports..." />
                                            <span className="search-dropdown__icon">
                                                <i className="zmdi zmdi-search"></i>
                                            </span>
                                        </form>
                                    </div>
                                </div>
                                <div className="header-button-item has-noti js-item-menu">
                                    <i className="zmdi zmdi-notifications"></i>
                                    <div className="notifi-dropdown js-dropdown">
                                        <div className="notifi__title">
                                            <p>You have 3 Notifications</p>
                                        </div>
                                        <div className="notifi__item">
                                            <div className="bg-c1 img-cir img-40">
                                                <i className="zmdi zmdi-email-open"></i>
                                            </div>
                                            <div className="content">
                                                <p>You got a email notification</p>
                                                <span className="date">April 12, 2018 06:50</span>
                                            </div>
                                        </div>
                                        <div className="notifi__item">
                                            <div className="bg-c2 img-cir img-40">
                                                <i className="zmdi zmdi-account-box"></i>
                                            </div>
                                            <div className="content">
                                                <p>Your account has been blocked</p>
                                                <span className="date">April 12, 2018 06:50</span>
                                            </div>
                                        </div>
                                        <div className="notifi__item">
                                            <div className="bg-c3 img-cir img-40">
                                                <i className="zmdi zmdi-file-text"></i>
                                            </div>
                                            <div className="content">
                                                <p>You got a new file</p>
                                                <span className="date">April 12, 2018 06:50</span>
                                            </div>
                                        </div>
                                        <div className="notifi__footer">
                                          <Link to={{
                                            pathname: '/',
                                            state: { fromDashboard: true }
                                            }}>
                                            All notifications
                                          </Link>
                                        </div>
                                    </div>
                                </div>
                                <div className="header-button-item mr-0 js-sidebar-btn">
                                    <i className="zmdi zmdi-menu"></i>
                                </div>
                                <div className="setting-menu js-right-sidebar d-none d-lg-block">
                                    <div className="account-dropdown__body">
                                        <div className="account-dropdown__item">
                                          <Link to={{
                                          pathname: '/',
                                          state: { fromDashboard: true }
                                          }}>
                                                <i className="zmdi zmdi-account"></i>Account
                                          </Link>
                                        </div>
                                        <div className="account-dropdown__item">
                                          <Link to={{
                                          pathname: '/',
                                          state: { fromDashboard: true }
                                          }}>
                                                <i className="zmdi zmdi-settings"></i>Setting
                                          </Link>
                                        </div>
                                        <div className="account-dropdown__item">
                                            <Link to={{
                                            pathname: '/',
                                            state: { fromDashboard: true }
                                            }}>
                                                <i className="zmdi zmdi-money-box"></i>Billing
                                            </Link>
                                        </div>
                                    </div>
                                    <div className="account-dropdown__body">
                                        <div className="account-dropdown__item">
                                          <Link to={{
                                          pathname: '/',
                                          state: { fromDashboard: true }
                                          }}>
                                                <i className="zmdi zmdi-globe"></i>Language
                                          </Link>
                                        </div>
                                        <div className="account-dropdown__item">
                                          <Link to={{
                                          pathname: '/',
                                          state: { fromDashboard: true }
                                          }}>
                                                <i className="zmdi zmdi-pin"></i>Location
                                          </Link>
                                        </div>
                                        <div className="account-dropdown__item">
                                          <Link to={{
                                          pathname: '/',
                                          state: { fromDashboard: true }
                                          }}>
                                                <i className="zmdi zmdi-email"></i>Email
                                          </Link>
                                        </div>
                                        <div className="account-dropdown__item">
                                          <Link to={{
                                          pathname: '/',
                                          state: { fromDashboard: true }
                                          }}>
                                                <i className="zmdi zmdi-notifications"></i>Notifications
                                          </Link>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            {/* END HEADER DESKTOP */}

            {/* BREADCRUMB*/}
            <section className="au-breadcrumb m-t-75">
                <div className="section__content section__content--p30">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-md-12">
                                <div className="au-breadcrumb-content">
                                    <div className="au-breadcrumb-left">
                                        <span className="au-breadcrumb-span">You are here:</span>
                                        <ul className="list-unstyled list-inline au-breadcrumb__list">
                                            <li className="list-inline-item active">
                                              <Link to={{
                                                pathname: '/',
                                                state: { fromDashboard: true }
                                              }}>
                                              Admisnitracion Escolar
                                            </Link>
                                            </li>
                                            <li className="list-inline-item seprate">
                                                <span>/</span>
                                            </li>
                                            <li className="list-inline-item">Dashboard</li>
                                        </ul>
                                    </div>
                                    <button className="au-btn au-btn-icon au-btn--green">
                                        <i className="zmdi zmdi-plus"></i>add item</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            {/*END BREADCRUMB*/}

            {/* STATISTIC*/}
            <section className="statistic">
                <div className="section__content section__content--p30">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-md-6 col-lg-3">
                                <div className="statistic__item">
                                    <h2 className="number">10,368</h2>
                                    <span className="desc">members online</span>
                                    <div className="icon">
                                        <i className="zmdi zmdi-account-o"></i>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6 col-lg-3">
                                <div className="statistic__item">
                                    <h2 className="number">388,688</h2>
                                    <span className="desc">items sold</span>
                                    <div className="icon">
                                        <i className="zmdi zmdi-shopping-cart"></i>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6 col-lg-3">
                                <div className="statistic__item">
                                    <h2 className="number">1,086</h2>
                                    <span className="desc">this week</span>
                                    <div className="icon">
                                        <i className="zmdi zmdi-calendar-note"></i>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6 col-lg-3">
                                <div className="statistic__item">
                                    <h2 className="number">$1,060,386</h2>
                                    <span className="desc">total earnings</span>
                                    <div className="icon">
                                        <i className="zmdi zmdi-money"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            {/* END STATISTIC*/}



            <section>
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="copyright">
                                <p>Copyright © 2018. All rights reserved.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            {/* END PAGE CONTAINER*/}
        </div>
      </div>
    );
  };
}

export default AdmEsc;
