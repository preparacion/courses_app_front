import $ from 'jquery';
import Popper from 'popper.js';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
//component import
import Navbar from '../component/NavBar';
import Header from '../component/Header';
import ConfigGeneral from '../component/ConfigGeneral';
//component import

class Sedes extends Component {
  render (){
    return(
      <div>

        {/*<ConfigGeneral />menu de configuracion de tema*/}
        <div id="theme-wrapper">

    		<div id="page-wrapper" className="container">
    			<div className="row">
    			<Navbar activo={1}/>
    				<div id="content-wrapper">
    					<div className="row">
    						<div className="col-lg-12">
    							<div className="row">
    									<Header />
    							</div>
                  <div className="row">
                      <div class="pull-left hidden-xs">
  											<div class="xs-graph pull-left">
  												<div class="graph-label">
  													<img  src="img/pople.png" style={{height:'60px'}} alt=""/>
  												</div>
  											</div>
                        <div class="md-graph pull-left mrg-l-lg mrg-r-sm">
  												<div class="graph-labelCustc">
  													<b style={{fontWeight:300}}>Administracion escolar</b>
  												</div>
  											</div>
  											<div class="md-graph pull-left mrg-l-lg mrg-r-sm">
  												<div class="graph-label">
  													<b>|</b> Sedes
  												</div>
  											</div>
  										</div>
                  </div>
    							<div className="row">
                    <div className="col-lg-8">

                    </div>
                    <div className="col-lg-2">

                    </div>

    							</div>
    						</div>
    					</div>

    					<footer id="footer-bar" className="row">
    						<p id="footer-copyright" className="col-xs-12">

    						</p>
    					</footer>
    				</div>
    			</div>
    		</div>
    	</div>
    </div>
    );
  };
}

export default Sedes;
